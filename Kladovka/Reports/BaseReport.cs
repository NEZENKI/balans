using System;
using System.Data;
using Microsoft.Reporting.WinForms;
using System.Collections.Generic;

namespace Balans.Reports
{
    public class BaseReport
    {
        private string _dataSourceName = @"ReportDataSet";

        protected BaseReport() { }


        protected virtual string DataSourceName
        {
            get { return _dataSourceName; }
            set { _dataSourceName = value; }
        }


        protected virtual string GetNamespaceWithNameRdlcFile(string nameFile)
        {
            if (string.IsNullOrEmpty(nameFile))
            {
                throw new ArgumentException("��� ����� ������ ������ ���� ������");
            }
            return string.Format("Balans.Reports.rdlc.{0}.rdlc", nameFile);
        }


        protected ReportViewer GetInitViewer(string rdlcFileName, DataTable dt)
        {
            var reportViewer = new ReportViewer();
            var reportDataSource = new ReportDataSource(DataSourceName, dt);

            reportViewer.LocalReport.DataSources.Add(reportDataSource);
            reportViewer.LocalReport.ReportEmbeddedResource = GetNamespaceWithNameRdlcFile(rdlcFileName);

            return reportViewer;
        }

        protected void AddDataSource(string dataSourceName, DataTable dt,
             ReportViewer reportViewer)
        {
            var reportDataSource = new ReportDataSource(dataSourceName, dt);
            reportViewer.LocalReport.DataSources.Add(reportDataSource);
        }

        protected string MonthTostring(int num)
        {
            string result = "";

            switch (num)
            {
                case 1: result = "������"; break;
                case 2: result = "�������"; break;
                case 3: result = "����"; break;
                case 4: result = "������"; break;
                case 5: result = "���"; break;
                case 6: result = "����"; break;
                case 7: result = "����"; break;
                case 8: result = "������"; break;
                case 9: result = "��������"; break;
                case 10: result = "�������"; break;
                case 11: result = "������"; break;
                case 12: result = "�������"; break;
                case 13: result = "������"; break;
                default: break;
            }
            return result;
        }
        protected string MonthTostring_(int num)
        {
            string result = "";

            switch (num)
            {
                case 1: result = "������"; break;
                case 2: result = "�������"; break;
                case 3: result = "�����"; break;
                case 4: result = "������"; break;
                case 5: result = "���"; break;
                case 6: result = "����"; break;
                case 7: result = "����"; break;
                case 8: result = "�������"; break;
                case 9: result = "��������"; break;
                case 10: result = "�������"; break;
                case 11: result = "������"; break;
                case 12: result = "�������"; break;
                case 13: result = "������"; break;
                default: break;
            }
            return result;
        }
    }
}
