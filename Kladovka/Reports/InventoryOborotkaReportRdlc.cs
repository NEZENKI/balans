using Balans.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;

namespace Balans.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class InventoryOborotkaReportRdlc : BaseReport
    {
        private readonly IEnumerable<KARTOTEKA.KARTOTEKARow> _reportKartoteka;

        int kcexk;
        string naimk;

        public InventoryOborotkaReportRdlc(IEnumerable<KARTOTEKA.KARTOTEKARow> reportKartoteka, int kklad, string naimk)
        {
            this.kcexk = kklad;
            this.naimk = naimk;
            _reportKartoteka = reportKartoteka;

            PrintReport.ShowReport(GetViewer(_reportKartoteka));
        }

        private ReportViewer GetViewer(IEnumerable<KARTOTEKA.KARTOTEKARow> kartotekaCalculates)
        {
            KARTOTEKA.KARTOTEKADataTable dt = new KARTOTEKA.KARTOTEKADataTable();

            foreach (var item in kartotekaCalculates)
            {
                var kartotekaRow = dt.NewKARTOTEKARow();
                kartotekaRow.ID = item.ID;
                kartotekaRow.ID_LISTKLAD = item.ID_LISTKLAD;
                kartotekaRow.NCARDS = item.NCARDS;
                kartotekaRow.OST1 = item.OST1;
                kartotekaRow.OST2 = item.OST2;
                kartotekaRow.NAIM = item.NAIM;
                kartotekaRow.SIGN = item.SIGN;
                kartotekaRow.CENA = item.CENA;
                kartotekaRow.NN = item.NN;
                kartotekaRow.KEI = item.KEI;
                kartotekaRow.PRIH = item.PRIH;
                kartotekaRow.RASH = item.RASH;
                kartotekaRow.CENAS = item.CENAS;
                kartotekaRow.DDAT = item.DDAT;
                kartotekaRow.CHET = item.CHET;
                kartotekaRow.RASHC = item.RASHC;

                dt.Rows.Add(kartotekaRow);
            }
            ReportViewer reportViewer = GetInitViewer("InventoryOborotkaReport", dt);

            naimk = naimk.Trim(' ');

            var rp = new ReportParameter("repPar_division", '"' + naimk + '"' + " �� " + kcexk.ToString()+" ����");
            reportViewer.LocalReport.SetParameters(rp);

            reportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(subreportProcessing);  

            return reportViewer;
        }

        private void subreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ReportDataSet", _reportKartoteka));
        }

        public static string NumberToString(long number, int mode)
        {
            string words = "";
            string endStr = "";
            string numberStr = number.ToString();
            for (int i = 0, r = 0; i < numberStr.Length; i++)
            {
                if (r == 3)
                    r = 0;
                if (i == 3)
                    mode = -1;
                if (i == 6)
                {
                    mode = -2;
                }
                if (i == 9)
                {
                    mode = -3;
                }
                if (r == 0)
                {
                    string check;
                    if ((numberStr.Length - 2 - i) >= 0)
                    {
                        if (numberStr[numberStr.Length - 2 - i].ToString() == "1")
                        {
                            check = numberStr[numberStr.Length - 2 - i].ToString()
                                    + numberStr[numberStr.Length - 1 - i].ToString();
                            i++;
                            r++;
                        }
                        else
                        {
                            check = numberStr[numberStr.Length - 1 - i].ToString();
                        }
                    }
                    else
                    {
                        check = numberStr[numberStr.Length - 1 - i].ToString();
                    }
                    if (mode == 0)
                        endStr = "";
                    else if (mode == 1)
                        endStr = "������ ";
                    else if (mode == 2)
                        endStr = "����� ";
                    else if (mode == 3)
                        endStr = "�������� ";
                    else if (mode == -1)
                        endStr = "����� ";
                    else if (mode == -2)
                        endStr = "��������� ";
                    else if (mode == -3)
                        endStr = "���������� ";
                    switch (check)
                    {
                        case "0":
                            if (mode == 0 && numberStr.Length == 1)
                                words = "����" + words;
                            else if (mode == 0)
                                words = "" + words;
                            else if (mode == 1 && i == 0 && numberStr.Length == 1)
                                words = "���� ������ " + words;
                            else if (mode == 1 && i == 0)
                                words = "������ " + words;
                            else if (mode == -1)
                                words = "����� " + words;
                            else if (mode == -2)
                                words = "��������� " + words;
                            else if (mode == -3)
                                words = "���������� " + words;
                            else if (mode == 2 && i == 0 && numberStr.Length == 1)
                                words = "���� ����� " + words;
                            else if (mode == 2 && i == 0)
                                words = "����� " + words;
                            else if (mode == 3 && i == 0 && number != 0)
                                words = "�������� " + words;
                            else if (mode == 3)
                                words = "" + words;
                            break;
                        case "1":
                            if (mode == 0)
                                words = "���� " + words;
                            else if (mode == 1 && i == 0)
                                words = "���� ����� " + words;
                            else if (mode == 2 && i == 0)
                                words = "���� ����� " + words;
                            else if (mode == 3 && i == 0)
                                words = "���� �������� " + words;
                            else if (mode == -1)
                                words = "���� ������ " + words;
                            else if (mode == -2)
                                words = "���� ������� " + words;
                            else if (mode == -3)
                                words = "���� �������� " + words;
                            break;
                        case "2":
                            if (mode == 0)
                                words = "��� " + words;
                            else if (mode == 1 && i == 0)
                                words = "��� ����� " + words;
                            else if (mode == 2 && i == 0)
                                words = "��� ����� " + words;
                            else if (mode == 3 && i == 0)
                                words = "��� �������� " + words;
                            else if (mode == -1)
                                words = "��� ������ " + words;
                            else if (mode == -2)
                                words = "��� �������� " + words;
                            else if (mode == -3)
                                words = "��� ��������� " + words;
                            break;
                        case "3":
                            if (mode == 0)
                                words = "��� " + words;
                            else if (mode == 1 && i == 0)
                                words = "��� ����� " + words;
                            else if (mode == 2 && i == 0)
                                words = "��� ����� " + words;
                            else if (mode == 3 && i == 0)
                                words = "��� �������� " + words;
                            else if (mode == -1)
                                words = "��� ������ " + words;
                            else if (mode == -2)
                                words = "��� �������� " + words;
                            else if (mode == -3)
                                words = "��� ��������� " + words;
                            break;
                        case "4":
                            if (mode == 0)
                                words = "������ " + words;
                            else if (mode == 1 && i == 0)
                                words = "������ ����� " + words;
                            else if (mode == 2 && i == 0)
                                words = "������ ����� " + words;
                            else if (mode == 3 && i == 0)
                                words = "������ �������� " + words;
                            else if (mode == -1)
                                words = "������ ������ " + words;
                            else if (mode == -2)
                                words = "������ �������� " + words;
                            else if (mode == -3)
                                words = "������ ��������� " + words;
                            break;
                        case "5":
                            words = "���� " + endStr + words;
                            break;
                        case "6":
                            words = "����� " + endStr + words;
                            break;
                        case "7":
                            words = "���� " + endStr + words;
                            break;
                        case "8":
                            words = "������ " + endStr + words;
                            break;
                        case "9":
                            words = "������ " + endStr + words;
                            break;
                        case "10":
                            words = "������ " + endStr + words;
                            break;
                        case "11":
                            words = "����������� " + endStr + words;
                            break;
                        case "12":
                            words = "���������� " + endStr + words;
                            break;
                        case "13":
                            words = "���������� " + endStr + words;
                            break;
                        case "14":
                            words = "������������ " + endStr + words;
                            break;
                        case "15":
                            words = "���������� " + endStr + words;
                            break;
                        case "16":
                            words = "����������� " + endStr + words;
                            break;
                        case "17":
                            words = "���������� " + endStr + words;
                            break;
                        case "18":
                            words = "������������ " + endStr + words;
                            break;
                        case "19":
                            words = "������������ " + endStr + words;
                            break;
                    }
                    r++;
                    continue;
                }
                if (r == 1)
                {
                    string check = "";
                    check = numberStr[numberStr.Length - 1 - i].ToString();
                    switch (check)
                    {
                        case "0":
                            words = "" + words;
                            break;
                        case "2":
                            words = "�������� " + words;
                            break;
                        case "3":
                            words = "�������� " + words;
                            break;
                        case "4":
                            words = "����� " + words;
                            break;
                        case "5":
                            words = "��������� " + words;
                            break;
                        case "6":
                            words = "���������� " + words;
                            break;
                        case "7":
                            words = "��������� " + words;
                            break;
                        case "8":
                            words = "����������� " + words;
                            break;
                        case "9":
                            words = "��������� " + words;
                            break;
                    }
                    r++;
                    continue;
                }
                if (r == 2)
                {
                    string check = "";
                    check = numberStr[numberStr.Length - 1 - i].ToString();
                    switch (check)
                    {
                        case "0":
                            words = "" + words;
                            break;
                        case "1":
                            words = "��� " + words;
                            break;
                        case "2":
                            words = "������ " + words;
                            break;
                        case "3":
                            words = "������ " + words;
                            break;
                        case "4":
                            words = "��������� " + words;
                            break;
                        case "5":
                            words = "������� " + words;
                            break;
                        case "6":
                            words = "�������� " + words;
                            break;
                        case "7":
                            words = "������� " + words;
                            break;
                        case "8":
                            words = "��������� " + words;
                            break;
                        case "9":
                            words = "��������� " + words;
                            break;
                    }
                    r++;
                }
            }
            return words;
        }

        public static string NumberToString(double number, int mode)
        {
            long cel = (long)number;
            long drob = Convert.ToInt64((number - (double)cel) * 1000);
            if (drob != 0)
                return NumberToString(cel, 2) + NumberToString(drob, 3);
            string numStr = NumberToString(cel, 2);
            return numStr.Remove(numStr.Length - 6);
        }
    }
}
