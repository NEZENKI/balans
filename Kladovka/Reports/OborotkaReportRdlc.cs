using Balans.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Balans.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class OborotkaReportRdlc : BaseReport
    {
        private readonly IEnumerable<KARTOTEKA.KARTOTEKARow> _reportKartoteka;

        int _kcexk;
        decimal _month, _year;
        string _naimk;

        public OborotkaReportRdlc(IEnumerable<KARTOTEKA.KARTOTEKARow> reportKartoteka, int kklad, string naimk, decimal month, decimal year)
        {
            _month = month;
            _year = year;
            _kcexk = kklad;
            _naimk = naimk.Trim(' ');
            _reportKartoteka = reportKartoteka;

            PrintReport.ShowReport(GetViewer(_reportKartoteka));
        }

        private ReportViewer GetViewer(IEnumerable<KARTOTEKA.KARTOTEKARow> kartotekaCalculates)
        {
            KARTOTEKA.KARTOTEKADataTable dt = new KARTOTEKA.KARTOTEKADataTable();

            foreach (var item in kartotekaCalculates.OrderBy(k => k.NCARDS).ThenBy(k => k.NAIM).ToList())
            {
                var kartotekaRow = dt.NewKARTOTEKARow();
                kartotekaRow.ID = item.ID;
                kartotekaRow.ID_LISTKLAD = item.ID_LISTKLAD;
                kartotekaRow.NCARDS = item.NCARDS;
                kartotekaRow.OST1 = item.OST1;
                kartotekaRow.OST2 = item.OST2;
                kartotekaRow.NAIM = item.NAIM;
                kartotekaRow.SIGN = item.SIGN;
                kartotekaRow.CENA = item.CENA;
                kartotekaRow.NN = item.NN;
                kartotekaRow.KEI = item.KEI;
                kartotekaRow.PRIH = item.PRIH;
                kartotekaRow.RASH = item.RASH;
                kartotekaRow.CENAS = item.CENAS;
                kartotekaRow.DDAT = item.DDAT;
                kartotekaRow.CHET = item.CHET;                
                kartotekaRow.RASHC = item.RASHC;

                dt.Rows.Add(kartotekaRow);
            }
            ReportViewer reportViewer = GetInitViewer("OborotkaReport", dt);

            string divString = "������������ ����� �� �������� " + '"' + _naimk + '"' + " �� ";
            divString += _kcexk.ToString() + " ����";
            divString += " �� " + MonthTostring(Convert.ToInt32(_month)) + ' ' + _year.ToString() + " �." + "(���� "+ dt.Rows[0].Field<string>("CHET").Trim(' ') + ')' ;
            var rp = new ReportParameter("repPar_division", divString);
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_date", '"' + _naimk + '"' + "01."+ _month.ToString() + '.' + _year.ToString());
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_monthEnd", MonthTostring_((int)_month));
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_monthStart", MonthTostring_((int)_month + 1));
            reportViewer.LocalReport.SetParameters(rp);

            return reportViewer;
        }
    }
}