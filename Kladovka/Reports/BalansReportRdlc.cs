using Balans.DataSets;
using Balans.DataSets.RASHNAMETableAdapters;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Balans.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class BalansReportRdlc : BaseReport
    {
        private readonly IEnumerable<BALANSMSP.BALANSMSPRow> _reportKartoteka;

        int _kcexk;
        decimal _month, _year;
        string _naimk;

        public BalansReportRdlc(IEnumerable<BALANSMSP.BALANSMSPRow> reportKartoteka, int kklad, string naimk, decimal month, decimal year)
        {
            _month = month;
            _year = year;
            _kcexk = kklad;
            _naimk = naimk;
            _reportKartoteka = reportKartoteka;

            PrintReport.ShowReport(GetViewer(_reportKartoteka));
        }

        private ReportViewer GetViewer(IEnumerable<BALANSMSP.BALANSMSPRow> kartotekaCalculates)
        {
            BALANSMSP.BALANSMSPDataTable dt = new BALANSMSP.BALANSMSPDataTable();

            foreach (var item in kartotekaCalculates)
            {
                BALANSMSP.BALANSMSPRow balansRow = dt.NewBALANSMSPRow();
                balansRow.ID = item.ID;
                balansRow.ID_LISTKLAD = item.ID_LISTKLAD;
                balansRow.NCARDS = item.NCARDS;
                balansRow.OST1 = item.OST1;
                balansRow.OST2 = item.OST2;
                balansRow.NAIM = item.NAIM;
                balansRow.SIGN = item.SIGN;
                balansRow.CENA = item.CENA;
                balansRow.KEI = item.KEI;
                balansRow.PRIH = item.PRIH;
                balansRow.CENAS = item.CENAS;
                balansRow.DDAT = item.DDAT;
                balansRow.CHET = item.CHET;
                balansRow.RASH1 = item.RASH1;
                balansRow.RASH2 = item.RASH2;
                balansRow.RASH3 = item.RASH3;
                balansRow.RASH4 = item.RASH4;
                balansRow.RASH5 = item.RASH5;
                balansRow.RASH6 = item.RASH6;
                balansRow.RASH7 = item.RASH7;
                balansRow.RASH8 = item.RASH8;
                balansRow.RASH9 = item.RASH9;
                balansRow.RASH10 = item.RASH10;
                balansRow.BRAK = item.BRAK;

                dt.Rows.Add(balansRow);
            }
            ReportViewer reportViewer = GetInitViewer("BalansReport", dt);

            _naimk = _naimk.Trim(' ');

            string divString = "";

            if (_naimk.Substring(0,6).ToLower() == "������")
            {
                divString = "������ \"" + _naimk + "\" �� ";
            }
            else
            {
                divString = "������ �������� \"" + _naimk + "\" �� ";
            }
            
            divString += _kcexk.ToString() + " ����";
            divString += " �� " + MonthTostring(Convert.ToInt32(_month)) + ' ' + _year.ToString() + " �.";
            
            var rp = new ReportParameter("repPar_division", divString);
            reportViewer.LocalReport.SetParameters(rp);

            RASHNAMETableAdapter rashNameAdapter = new RASHNAMETableAdapter();
            List<RASHNAME.RASHNAMERow> rashNameList = rashNameAdapter.GetDataByLISTKLAD(dt.Rows[0].Field<decimal>("ID_LISTKLAD")).OrderBy(r => r.ID).ToList();

            if (rashNameList.Count > 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    rp = new ReportParameter("repPar_name" + (i * 3 + 1).ToString(), rashNameList[i].NAME1);
                    reportViewer.LocalReport.SetParameters(rp);
                    rp = new ReportParameter("repPar_name" + (i * 3 + 2).ToString(), rashNameList[i].NAME2);
                    reportViewer.LocalReport.SetParameters(rp);
                    rp = new ReportParameter("repPar_name" + (i * 3 + 3).ToString(), rashNameList[i].NAME3);
                    reportViewer.LocalReport.SetParameters(rp);
                }
            }
            rp = new ReportParameter("repPar_date", _naimk + "01." + _month.ToString() + '.' + _year.ToString());
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_monthStart", "01." + _month.ToString() + '.' + _year.ToString());
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_monthEnd", "01." +  (_month==12? '1' + '.' + (_year+1).ToString(): (_month + 1).ToString() + '.' + _year.ToString()));
            reportViewer.LocalReport.SetParameters(rp);

            return reportViewer;
        }
    }
}