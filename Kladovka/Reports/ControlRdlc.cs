using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Reporting.WinForms;
using Balans.DataSets;
using System.Globalization;
using System.Windows.Forms;
using Balans.DataSets.RASHNAMETableAdapters;
using System.Linq;
using Balans.DataSets.KARTOTEKATableAdapters;

namespace Balans.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class ControlRdlc : BaseReport
    {
        private readonly IEnumerable<BALANSMSP.BALANSMSPRow> _reportKartoteka;

        int kcexk;
        string naimk;

        public ControlRdlc(IEnumerable<BALANSMSP.BALANSMSPRow> reportKartoteka, int kklad, string naimk)
        {
            this.kcexk = kklad;
            this.naimk = naimk;
            _reportKartoteka = reportKartoteka;

            PrintReport.ShowReport(GetViewer(_reportKartoteka));
        }

        private ReportViewer GetViewer(IEnumerable<BALANSMSP.BALANSMSPRow> kartotekaCalculates)
        {
            BALANSMSP.BALANSMSPDataTable dt = new BALANSMSP.BALANSMSPDataTable();

            foreach (var item in kartotekaCalculates)
            {
                BALANSMSP.BALANSMSPRow balansRow = dt.NewBALANSMSPRow();
                balansRow.ID = item.ID;
                balansRow.ID_LISTKLAD = item.ID_LISTKLAD;
                balansRow.NCARDS = item.NCARDS;
                balansRow.OST1 = item.OST1;
                balansRow.OST2 = item.OST2;
                balansRow.NAIM = item.NAIM;
                balansRow.SIGN = item.SIGN;
                balansRow.CENA = item.CENA;
                balansRow.KEI = item.KEI;
                balansRow.PRIH = item.PRIH;
                balansRow.CENAS = item.CENAS;
                balansRow.DDAT = item.DDAT;
                balansRow.CHET = item.CHET;
                balansRow.RASH1 = item.RASH1;
                balansRow.RASH2 = item.RASH2;
                balansRow.RASH3 = item.RASH3;
                balansRow.RASH4 = item.RASH4;
                balansRow.RASH5 = item.RASH5;
                balansRow.RASH6 = item.RASH6;
                balansRow.RASH7 = item.RASH7;
                balansRow.RASH8 = item.RASH8;
                balansRow.RASH9 = item.RASH9;
                balansRow.RASH10 = item.RASH10;
                balansRow.BRAK = item.BRAK;

                dt.Rows.Add(balansRow);
            }
            ReportViewer reportViewer = GetInitViewer("Control", dt);

            reportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(subreportProcessing);
            return reportViewer;
        }

        private void subreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            KARTOTEKATableAdapter kartoteka = new KARTOTEKATableAdapter();
            KARTOTEKA.KARTOTEKADataTable kartotekaData = kartoteka.GetDataByListklad(_reportKartoteka.First().ID_LISTKLAD);

            KARTOTEKA.KARTOTEKADataTable dt = new KARTOTEKA.KARTOTEKADataTable();

            foreach (var item in kartotekaData)
            {
                var kartotekaRow = dt.NewKARTOTEKARow();
                kartotekaRow.ID = item.ID;
                kartotekaRow.ID_LISTKLAD = item.ID_LISTKLAD;
                kartotekaRow.NCARDS = item.NCARDS;
                kartotekaRow.OST1 = item.OST1 == null ? 0 : item.OST1;
                kartotekaRow.OST2 = item.OST2 == null ? 0 : item.OST2;
                kartotekaRow.NAIM = item.NAIM;
                kartotekaRow.SIGN = item.SIGN;
                kartotekaRow.CENA = item.CENA == null ? 0 : item.CENA;
                kartotekaRow.NN = item.NN;
                kartotekaRow.KEI = item.KEI;
                kartotekaRow.PRIH = item.PRIH;
                kartotekaRow.RASH = item.RASH;
                kartotekaRow.CENAS = item.CENAS;
                kartotekaRow.DDAT = item.DDAT;
                kartotekaRow.CHET = item.CHET;
                kartotekaRow.RASHC = item.RASHC == null ? 0 : item.RASHC;

                dt.Rows.Add(kartotekaRow);
            }

            e.DataSources.Add(new ReportDataSource("ReportDataSet", dt.ToList()));
        }
    }

}