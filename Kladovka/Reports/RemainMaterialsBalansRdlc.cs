using Balans.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;

namespace Balans.Reports
{
    public class RemainMaterialsBalansRdlc : BaseReport
    {
        private readonly IEnumerable<BALANSMSP.BALANSMSPRow> _reportKartoteka;

        int _kcexk;
        decimal _month, _year;
        string _naimk;

        public RemainMaterialsBalansRdlc(IEnumerable<BALANSMSP.BALANSMSPRow> reportKartoteka, int kklad, string naimk, decimal month, decimal year)
        {
            _month = month;
            _year = year;
            _kcexk = kklad;
            _naimk = naimk.Trim(' ');
            _reportKartoteka = reportKartoteka;

            PrintReport.ShowReport(GetViewer(_reportKartoteka));
        }

        private ReportViewer GetViewer(IEnumerable<BALANSMSP.BALANSMSPRow> balansCalculates)
        {
            BALANSMSP.BALANSMSPDataTable dt = new BALANSMSP.BALANSMSPDataTable();

            foreach (var item in balansCalculates)
            {
                BALANSMSP.BALANSMSPRow balansRow = dt.NewBALANSMSPRow();
                balansRow.ID = item.ID;
                balansRow.ID_LISTKLAD = item.ID_LISTKLAD;
                balansRow.NCARDS = item.NCARDS;
                balansRow.OST1 = item.OST1;
                balansRow.OST2 = item.OST2;
                balansRow.NAIM = item.NAIM;
                balansRow.SIGN = item.SIGN;
                balansRow.CENA = item.CENA;
                balansRow.KEI = item.KEI;
                balansRow.PRIH = item.PRIH;
                balansRow.CENAS = item.CENAS;
                balansRow.DDAT = item.DDAT;
                balansRow.CHET = item.CHET;
                balansRow.RASH1 = item.RASH1;
                balansRow.RASH2 = item.RASH2;
                balansRow.RASH3 = item.RASH3;
                balansRow.RASH4 = item.RASH4;
                balansRow.RASH5 = item.RASH5;
                balansRow.RASH6 = item.RASH6;
                balansRow.RASH7 = item.RASH7;
                balansRow.RASH8 = item.RASH8;
                balansRow.RASH9 = item.RASH9;
                balansRow.RASH10 = item.RASH10;
                balansRow.BRAK = item.BRAK;

                dt.Rows.Add(balansRow);
            }
            ReportViewer reportViewer = GetInitViewer("RemainMaterialsBalans", dt);            

            string divString = "������� ���������� ������� �� �������� " + '"' + _naimk + '"' + " �� " + _kcexk.ToString() + " ����";
            divString+= " �� " + MonthTostring(Convert.ToInt32(_month)) + ' ' + _year.ToString() + " �.";
            var rp = new ReportParameter("repPar_division", divString);
            reportViewer.LocalReport.SetParameters(rp);
            rp = new ReportParameter("repPar_date", "01."+ _month.ToString() + '.' + _year.ToString());
            reportViewer.LocalReport.SetParameters(rp);

            return reportViewer;
        }
    }
}