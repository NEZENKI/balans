using Balans.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;

namespace Balans.Reports
{
    public class RemainMaterialsOborotkaIndustriesRdlc : BaseReport
    {
        private readonly IEnumerable<ReportModel.REMAININDUSTRIESRow> _reportData;

        int kcexk;
        string naimk;
        decimal _month, _year;

        public RemainMaterialsOborotkaIndustriesRdlc(IEnumerable<ReportModel.REMAININDUSTRIESRow> reportIndustries, int kklad, string naimk, decimal month , decimal year)
        {
            this.kcexk = kklad;
            this.naimk = naimk;
            _month = month;
            _year = year;

            _reportData = reportIndustries;           
            PrintReport.ShowReport(GetViewer(_reportData));
        }

        private ReportViewer GetViewer(IEnumerable<ReportModel.REMAININDUSTRIESRow> kartotekaCalculates)
        {
            ReportModel.REMAININDUSTRIESDataTable dt = new ReportModel.REMAININDUSTRIESDataTable();

            foreach (var item in kartotekaCalculates)
            {
                var kartotekaRow = dt.NewREMAININDUSTRIESRow();

                kartotekaRow.RASH = item.RASH;
                kartotekaRow.NAME = item.NAME.Trim(' ');
                kartotekaRow.SHORTNAME = item.SHORTNAME.Trim(' ');
                kartotekaRow.CENA = item.CENA;
                kartotekaRow.INDUSTRY = item.INDUSTRY;

                dt.Rows.Add(kartotekaRow);
            }
            ReportViewer reportViewer = GetInitViewer("RemainMaterialsOborotkaIndustries", dt);

            naimk = naimk.Trim(' ');

            string divString = '"' + naimk + '"' + " �� " + kcexk.ToString() + " ����" + " �� " + MonthTostring(Convert.ToInt32(_month)) + ' ' + _year.ToString() + " ����.";
            var rp = new ReportParameter("repPar_division", divString);
            reportViewer.LocalReport.SetParameters(rp);           

            return reportViewer;
        }     
    }
}