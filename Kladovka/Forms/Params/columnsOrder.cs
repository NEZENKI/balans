﻿using ColumnsPropertiesNamespace;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Balans.Forms.Params
{
    public partial class ColumnsOrder : Form
    {
        ColumnsProperties _prop;
        public ColumnsOrder(ColumnsProperties prop)
        {
            _prop = prop;
            InitializeComponent();
        }

        private void ColumnsOrder_Load(object sender, EventArgs e)
        {
            columnsListBox.Items.AddRange(_prop.Columns.OrderBy(p=>p.DisplayIndex).Select(p => p.DisplayName).ToArray());
            columnsListBox.SelectedIndex = 0;
        }

        private void ColumnsOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            int i = 0;
            foreach (var col in columnsListBox.Items)
            {
                _prop.FindByDisplayName(col.ToString()).DisplayIndex = i++;
            }
        }

        private void moveUp_Click(object sender, EventArgs e)
        {
            int index = columnsListBox.SelectedIndex;
            if (index > 0)
            {
                var item = columnsListBox.SelectedItem;
                columnsListBox.Items.RemoveAt(index);
                columnsListBox.Items.Insert(index - 1, item);
                columnsListBox.SelectedIndex = index - 1;
            }
        }

        private void moveDown_Click(object sender, EventArgs e)
        {
            int index = columnsListBox.SelectedIndex;
            if (index < columnsListBox.Items.Count)
            {
                var item = columnsListBox.SelectedItem;
                columnsListBox.Items.RemoveAt(index);
                columnsListBox.Items.Insert(index + 1, item);
                columnsListBox.SelectedIndex = index + 1;
            }
        }

        private void MoveToStart_Click(object sender, EventArgs e)
        {
            int index = columnsListBox.SelectedIndex;
            if (index > 0)
            {
                var item = columnsListBox.SelectedItem;
                columnsListBox.Items.RemoveAt(index);
                columnsListBox.Items.Insert(0, item);
                columnsListBox.SelectedIndex = 0;
            }
        }

        private void MoveToEnd_Click(object sender, EventArgs e)
        {
            int index = columnsListBox.SelectedIndex;
            if (index < columnsListBox.Items.Count)
            {
                var item = columnsListBox.SelectedItem;
                columnsListBox.Items.RemoveAt(index);
                columnsListBox.Items.Insert(columnsListBox.Items.Count, item);
                columnsListBox.SelectedIndex = columnsListBox.Items.Count - 1;
            }
        }
    }
}
