﻿namespace Balans
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTableContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.поискToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объединитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обнулениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перенумерацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.начатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьССохранниемИзмененийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьБезСохраненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкаОтображенияСтолбцовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обновитьДанныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTReserveCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обработкаЗаписиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поискToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объединениеКарточекToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обнудениеЦеныприToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обработкаИнформацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переходНаНовыйМесяцToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пустографкаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.подготовкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шапкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обороткаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрольToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.отчётПоБалансуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвентаризацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.остаткиПоБалансуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пустографкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.минимальнаяЗарплатаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникЕдиницИзмеренияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникКладовыхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sssToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.naimk = new System.Windows.Forms.ComboBox();
            this.kkladLabel = new System.Windows.Forms.Label();
            this.naimkLabel = new System.Windows.Forms.Label();
            this.kcexk = new System.Windows.Forms.ComboBox();
            this.month = new System.Windows.Forms.NumericUpDown();
            this.year = new System.Windows.Forms.NumericUpDown();
            this.rdataLabel = new System.Windows.Forms.Label();
            this.ost1Check = new System.Windows.Forms.CheckBox();
            this.mainDataGrid = new System.Windows.Forms.DataGridView();
            this.searchText = new System.Windows.Forms.TextBox();
            this.searchBox = new System.Windows.Forms.GroupBox();
            this.MainTableContextMenu.SuspendLayout();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.month)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).BeginInit();
            this.searchBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTableContextMenu
            // 
            this.MainTableContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поискToolStripMenuItem1,
            this.удалитьToolStripMenuItem,
            this.добавитьToolStripMenuItem,
            this.копироватьToolStripMenuItem,
            this.объединитьToolStripMenuItem,
            this.обнулениеToolStripMenuItem,
            this.перенумерацияToolStripMenuItem,
            this.настройкаОтображенияСтолбцовToolStripMenuItem});
            this.MainTableContextMenu.Name = "MainTableContextMenu";
            this.MainTableContextMenu.Size = new System.Drawing.Size(266, 180);
            // 
            // поискToolStripMenuItem1
            // 
            this.поискToolStripMenuItem1.Name = "поискToolStripMenuItem1";
            this.поискToolStripMenuItem1.Size = new System.Drawing.Size(265, 22);
            this.поискToolStripMenuItem1.Text = "Поиск";
            this.поискToolStripMenuItem1.Click += new System.EventHandler(this.поискToolStripMenuItem1_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.удалитьToolStripMenuItem.Text = "Удаление";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.добавитьToolStripMenuItem.Text = "Вставка";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.добавитьToolStripMenuItem_Click);
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.копироватьToolStripMenuItem.Text = "Копирование";
            this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.копироватьToolStripMenuItem_Click);
            // 
            // объединитьToolStripMenuItem
            // 
            this.объединитьToolStripMenuItem.Name = "объединитьToolStripMenuItem";
            this.объединитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.объединитьToolStripMenuItem.Text = "Объединение карточек";
            this.объединитьToolStripMenuItem.Click += new System.EventHandler(this.объединитьToolStripMenuItem_Click);
            // 
            // обнулениеToolStripMenuItem
            // 
            this.обнулениеToolStripMenuItem.Name = "обнулениеToolStripMenuItem";
            this.обнулениеToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.обнулениеToolStripMenuItem.Text = "Обнуление";
            this.обнулениеToolStripMenuItem.Click += new System.EventHandler(this.обнулениеToolStripMenuItem_Click);
            // 
            // перенумерацияToolStripMenuItem
            // 
            this.перенумерацияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.начатьToolStripMenuItem,
            this.завершитьССохранниемИзмененийToolStripMenuItem,
            this.завершитьБезСохраненияToolStripMenuItem});
            this.перенумерацияToolStripMenuItem.Name = "перенумерацияToolStripMenuItem";
            this.перенумерацияToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.перенумерацияToolStripMenuItem.Text = "Перенумерация";
            // 
            // начатьToolStripMenuItem
            // 
            this.начатьToolStripMenuItem.Name = "начатьToolStripMenuItem";
            this.начатьToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.начатьToolStripMenuItem.Text = "Начать";
            this.начатьToolStripMenuItem.Click += new System.EventHandler(this.начатьToolStripMenuItem_Click);
            // 
            // завершитьССохранниемИзмененийToolStripMenuItem
            // 
            this.завершитьССохранниемИзмененийToolStripMenuItem.Name = "завершитьССохранниемИзмененийToolStripMenuItem";
            this.завершитьССохранниемИзмененийToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.завершитьССохранниемИзмененийToolStripMenuItem.Text = "Завершить с сохраннием";
            this.завершитьССохранниемИзмененийToolStripMenuItem.Click += new System.EventHandler(this.завершитьССохранниемИзмененийToolStripMenuItem_Click);
            // 
            // завершитьБезСохраненияToolStripMenuItem
            // 
            this.завершитьБезСохраненияToolStripMenuItem.Name = "завершитьБезСохраненияToolStripMenuItem";
            this.завершитьБезСохраненияToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.завершитьБезСохраненияToolStripMenuItem.Text = "Завершить без сохранения";
            this.завершитьБезСохраненияToolStripMenuItem.Click += new System.EventHandler(this.завершитьБезСохраненияToolStripMenuItem_Click);
            // 
            // настройкаОтображенияСтолбцовToolStripMenuItem
            // 
            this.настройкаОтображенияСтолбцовToolStripMenuItem.Name = "настройкаОтображенияСтолбцовToolStripMenuItem";
            this.настройкаОтображенияСтолбцовToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.настройкаОтображенияСтолбцовToolStripMenuItem.Text = "Настройка отображения столбцов";
            this.настройкаОтображенияСтолбцовToolStripMenuItem.Click += new System.EventHandler(this.настройкаОтображенияСтолбцовToolStripMenuItem_Click);
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.Silver;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.обработкаЗаписиToolStripMenuItem,
            this.обработкаИнформацииToolStripMenuItem,
            this.подготовкаToolStripMenuItem,
            this.обороткаToolStripMenuItem,
            this.отчетToolStripMenuItem,
            this.справкаToolStripMenuItem,
            this.sssToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1084, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menu";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьДанныеToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem,
            this.tESTReserveCopyToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // обновитьДанныеToolStripMenuItem
            // 
            this.обновитьДанныеToolStripMenuItem.Name = "обновитьДанныеToolStripMenuItem";
            this.обновитьДанныеToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.обновитьДанныеToolStripMenuItem.Text = "Обновить данные";
            this.обновитьДанныеToolStripMenuItem.Click += new System.EventHandler(this.обновитьДанныеToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // tESTReserveCopyToolStripMenuItem
            // 
            this.tESTReserveCopyToolStripMenuItem.Name = "tESTReserveCopyToolStripMenuItem";
            this.tESTReserveCopyToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.tESTReserveCopyToolStripMenuItem.Text = "TEST Backup";
            this.tESTReserveCopyToolStripMenuItem.Click += new System.EventHandler(this.TESTReserveCopyToolStripMenuItem_Click);
            // 
            // обработкаЗаписиToolStripMenuItem
            // 
            this.обработкаЗаписиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поискToolStripMenuItem,
            this.удалениеToolStripMenuItem,
            this.вставкаToolStripMenuItem,
            this.копированиеToolStripMenuItem,
            this.объединениеКарточекToolStripMenuItem,
            this.обнудениеЦеныприToolStripMenuItem,
            this.просчетToolStripMenuItem,
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem});
            this.обработкаЗаписиToolStripMenuItem.Name = "обработкаЗаписиToolStripMenuItem";
            this.обработкаЗаписиToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.обработкаЗаписиToolStripMenuItem.Text = "Обработка записи";
            // 
            // поискToolStripMenuItem
            // 
            this.поискToolStripMenuItem.Name = "поискToolStripMenuItem";
            this.поискToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.поискToolStripMenuItem.Text = "Поиск";
            this.поискToolStripMenuItem.Click += new System.EventHandler(this.поискToolStripMenuItem_Click);
            // 
            // удалениеToolStripMenuItem
            // 
            this.удалениеToolStripMenuItem.Name = "удалениеToolStripMenuItem";
            this.удалениеToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.удалениеToolStripMenuItem.Text = "Удаление";
            this.удалениеToolStripMenuItem.Click += new System.EventHandler(this.удалениеToolStripMenuItem_Click);
            // 
            // вставкаToolStripMenuItem
            // 
            this.вставкаToolStripMenuItem.Name = "вставкаToolStripMenuItem";
            this.вставкаToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.вставкаToolStripMenuItem.Text = "Вставка";
            this.вставкаToolStripMenuItem.Click += new System.EventHandler(this.вставкаToolStripMenuItem_Click);
            // 
            // копированиеToolStripMenuItem
            // 
            this.копированиеToolStripMenuItem.Name = "копированиеToolStripMenuItem";
            this.копированиеToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.копированиеToolStripMenuItem.Text = "Копирование";
            this.копированиеToolStripMenuItem.Click += new System.EventHandler(this.копированиеToolStripMenuItem_Click);
            // 
            // объединениеКарточекToolStripMenuItem
            // 
            this.объединениеКарточекToolStripMenuItem.Name = "объединениеКарточекToolStripMenuItem";
            this.объединениеКарточекToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.объединениеКарточекToolStripMenuItem.Text = "Объединение карточек";
            this.объединениеКарточекToolStripMenuItem.Click += new System.EventHandler(this.объединениеКарточекToolStripMenuItem_Click);
            // 
            // обнудениеЦеныприToolStripMenuItem
            // 
            this.обнудениеЦеныприToolStripMenuItem.Name = "обнудениеЦеныприToolStripMenuItem";
            this.обнудениеЦеныприToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.обнудениеЦеныприToolStripMenuItem.Text = "Обнуление цены(при ост=0)";
            this.обнудениеЦеныприToolStripMenuItem.Click += new System.EventHandler(this.обнулениеЦеныприToolStripMenuItem_Click);
            // 
            // просчетToolStripMenuItem
            // 
            this.просчетToolStripMenuItem.Name = "просчетToolStripMenuItem";
            this.просчетToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.просчетToolStripMenuItem.Text = "Просчет";
            this.просчетToolStripMenuItem.Click += new System.EventHandler(this.просчетToolStripMenuItem_Click);
            // 
            // очиститьВсеПолякромеСчетаИToolStripMenuItem
            // 
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem.Name = "очиститьВсеПолякромеСчетаИToolStripMenuItem";
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem.Text = "Очищение всех полей(кроме счета и номера карты)";
            this.очиститьВсеПолякромеСчетаИToolStripMenuItem.Click += new System.EventHandler(this.ClearAllFieldsEvent);
            // 
            // обработкаИнформацииToolStripMenuItem
            // 
            this.обработкаИнформацииToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem,
            this.переходНаНовыйМесяцToolStripMenuItem,
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem,
            this.пустографкаToolStripMenuItem1});
            this.обработкаИнформацииToolStripMenuItem.Name = "обработкаИнформацииToolStripMenuItem";
            this.обработкаИнформацииToolStripMenuItem.Size = new System.Drawing.Size(155, 20);
            this.обработкаИнформацииToolStripMenuItem.Text = "Обработка информации";
            // 
            // переносОстатковНаНачалоМесяцаToolStripMenuItem
            // 
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem.Name = "переносОстатковНаНачалоМесяцаToolStripMenuItem";
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem.Text = "Перенос остатков на начало месяца";
            this.переносОстатковНаНачалоМесяцаToolStripMenuItem.Click += new System.EventHandler(this.переносОстатковНаНачалоМесяцаToolStripMenuItem_Click);
            // 
            // переходНаНовыйМесяцToolStripMenuItem
            // 
            this.переходНаНовыйМесяцToolStripMenuItem.Name = "переходНаНовыйМесяцToolStripMenuItem";
            this.переходНаНовыйМесяцToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.переходНаНовыйМесяцToolStripMenuItem.Text = "Обьединение карточек и удаление цен";
            this.переходНаНовыйМесяцToolStripMenuItem.Click += new System.EventHandler(this.переходНаНовыйМесяцToolStripMenuItem_Click);
            // 
            // удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem
            // 
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem.Name = "удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem";
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem.Text = "Удаление карточек с нулевым остатком на 1 января";
            this.удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem.Click += new System.EventHandler(this.удалениеКарточекСНулевымОстаткомToolStripMenuItem_Click);
            // 
            // пустографкаToolStripMenuItem1
            // 
            this.пустографкаToolStripMenuItem1.Name = "пустографкаToolStripMenuItem1";
            this.пустографкаToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.пустографкаToolStripMenuItem1.Text = "Пустографки";
            this.пустографкаToolStripMenuItem1.Click += new System.EventHandler(this.пустографкаToolStripMenuItem1_Click);
            // 
            // подготовкаToolStripMenuItem
            // 
            this.подготовкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.шапкаToolStripMenuItem,
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem});
            this.подготовкаToolStripMenuItem.Name = "подготовкаToolStripMenuItem";
            this.подготовкаToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.подготовкаToolStripMenuItem.Text = "Подготовка";
            // 
            // шапкаToolStripMenuItem
            // 
            this.шапкаToolStripMenuItem.Name = "шапкаToolStripMenuItem";
            this.шапкаToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.шапкаToolStripMenuItem.Text = "Шапка";
            this.шапкаToolStripMenuItem.Click += new System.EventHandler(this.шапкаToolStripMenuItem_Click);
            // 
            // переносРасходовОбороткиВПриходнойБалансToolStripMenuItem
            // 
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem.Name = "переносРасходовОбороткиВПриходнойБалансToolStripMenuItem";
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem.Text = "Перенос расходов оборотки в приходной баланс";
            this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem.Click += new System.EventHandler(this.переносРасходовОбороткиВПриходнойБалансToolStripMenuItem_Click);
            // 
            // обороткаToolStripMenuItem
            // 
            this.обороткаToolStripMenuItem.ForeColor = System.Drawing.Color.Blue;
            this.обороткаToolStripMenuItem.Name = "обороткаToolStripMenuItem";
            this.обороткаToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.обороткаToolStripMenuItem.Text = "Оборотка";
            this.обороткаToolStripMenuItem.Click += new System.EventHandler(this.обороткаToolStripMenuItem_Click);
            // 
            // отчетToolStripMenuItem
            // 
            this.отчетToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.контрольToolStripMenuItem1,
            this.отчётПоБалансуToolStripMenuItem,
            this.инвентаризацияToolStripMenuItem,
            this.остаткиПоБалансуToolStripMenuItem,
            this.пустографкаToolStripMenuItem});
            this.отчетToolStripMenuItem.Name = "отчетToolStripMenuItem";
            this.отчетToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.отчетToolStripMenuItem.Text = "Отчет";
            // 
            // контрольToolStripMenuItem1
            // 
            this.контрольToolStripMenuItem1.Name = "контрольToolStripMenuItem1";
            this.контрольToolStripMenuItem1.Size = new System.Drawing.Size(230, 22);
            this.контрольToolStripMenuItem1.Text = "Контроль";
            this.контрольToolStripMenuItem1.Click += new System.EventHandler(this.контрольToolStripMenuItem1_Click);
            // 
            // отчётПоБалансуToolStripMenuItem
            // 
            this.отчётПоБалансуToolStripMenuItem.Name = "отчётПоБалансуToolStripMenuItem";
            this.отчётПоБалансуToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.отчётПоБалансуToolStripMenuItem.Text = "Отчёт по балансу";
            this.отчётПоБалансуToolStripMenuItem.Click += new System.EventHandler(this.отчётПоБалансуToolStripMenuItem_Click);
            // 
            // инвентаризацияToolStripMenuItem
            // 
            this.инвентаризацияToolStripMenuItem.Name = "инвентаризацияToolStripMenuItem";
            this.инвентаризацияToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.инвентаризацияToolStripMenuItem.Text = "Инвентаризация по балансу";
            this.инвентаризацияToolStripMenuItem.Click += new System.EventHandler(this.инвентаризацияToolStripMenuItem_Click);
            // 
            // остаткиПоБалансуToolStripMenuItem
            // 
            this.остаткиПоБалансуToolStripMenuItem.Name = "остаткиПоБалансуToolStripMenuItem";
            this.остаткиПоБалансуToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.остаткиПоБалансуToolStripMenuItem.Text = "Остатки по балансу";
            this.остаткиПоБалансуToolStripMenuItem.Click += new System.EventHandler(this.остаткиПоБалансуToolStripMenuItem_Click);
            // 
            // пустографкаToolStripMenuItem
            // 
            this.пустографкаToolStripMenuItem.Name = "пустографкаToolStripMenuItem";
            this.пустографкаToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.пустографкаToolStripMenuItem.Text = "Печать пустографки";
            this.пустографкаToolStripMenuItem.Click += new System.EventHandler(this.пустографкаToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.минимальнаяЗарплатаToolStripMenuItem,
            this.справочникЕдиницИзмеренияToolStripMenuItem,
            this.справочникКладовыхToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // минимальнаяЗарплатаToolStripMenuItem
            // 
            this.минимальнаяЗарплатаToolStripMenuItem.Name = "минимальнаяЗарплатаToolStripMenuItem";
            this.минимальнаяЗарплатаToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.минимальнаяЗарплатаToolStripMenuItem.Text = "Минимальная зарплата";
            this.минимальнаяЗарплатаToolStripMenuItem.Click += new System.EventHandler(this.минимальнаяЗарплатаToolStripMenuItem_Click);
            // 
            // справочникЕдиницИзмеренияToolStripMenuItem
            // 
            this.справочникЕдиницИзмеренияToolStripMenuItem.Name = "справочникЕдиницИзмеренияToolStripMenuItem";
            this.справочникЕдиницИзмеренияToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.справочникЕдиницИзмеренияToolStripMenuItem.Text = "Справочник единиц измерения";
            this.справочникЕдиницИзмеренияToolStripMenuItem.Click += new System.EventHandler(this.справочникЕдиницИзмеренияToolStripMenuItem_Click);
            // 
            // справочникКладовыхToolStripMenuItem
            // 
            this.справочникКладовыхToolStripMenuItem.Name = "справочникКладовыхToolStripMenuItem";
            this.справочникКладовыхToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.справочникКладовыхToolStripMenuItem.Text = "Справочник кладовых";
            this.справочникКладовыхToolStripMenuItem.Click += new System.EventHandler(this.справочникКладовыхToolStripMenuItem_Click);
            // 
            // sssToolStripMenuItem
            // 
            this.sssToolStripMenuItem.Name = "sssToolStripMenuItem";
            this.sssToolStripMenuItem.Size = new System.Drawing.Size(12, 20);
            // 
            // naimk
            // 
            this.naimk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.naimk.FormattingEnabled = true;
            this.naimk.Location = new System.Drawing.Point(327, 27);
            this.naimk.Name = "naimk";
            this.naimk.Size = new System.Drawing.Size(121, 21);
            this.naimk.TabIndex = 2;
            this.naimk.SelectedIndexChanged += new System.EventHandler(this.Naimk_SelectedIndexChanged);
            // 
            // kkladLabel
            // 
            this.kkladLabel.AutoSize = true;
            this.kkladLabel.Location = new System.Drawing.Point(2, 30);
            this.kkladLabel.Name = "kkladLabel";
            this.kkladLabel.Size = new System.Drawing.Size(52, 13);
            this.kkladLabel.TabIndex = 6;
            this.kkladLabel.Text = "Код цеха";
            // 
            // naimkLabel
            // 
            this.naimkLabel.AutoSize = true;
            this.naimkLabel.Location = new System.Drawing.Point(187, 30);
            this.naimkLabel.Name = "naimkLabel";
            this.naimkLabel.Size = new System.Drawing.Size(134, 13);
            this.naimkLabel.TabIndex = 5;
            this.naimkLabel.Text = "Наименование кладовой";
            // 
            // kcexk
            // 
            this.kcexk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.kcexk.FormattingEnabled = true;
            this.kcexk.Location = new System.Drawing.Point(60, 28);
            this.kcexk.Margin = new System.Windows.Forms.Padding(3, 3, 3, 1);
            this.kcexk.Name = "kcexk";
            this.kcexk.Size = new System.Drawing.Size(121, 21);
            this.kcexk.TabIndex = 1;
            this.kcexk.SelectedIndexChanged += new System.EventHandler(this.Kcexk_SelectedIndexChanged);
            // 
            // month
            // 
            this.month.Location = new System.Drawing.Point(567, 28);
            this.month.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.month.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.month.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.month.Name = "month";
            this.month.Size = new System.Drawing.Size(50, 20);
            this.month.TabIndex = 4;
            this.month.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // year
            // 
            this.year.Location = new System.Drawing.Point(618, 28);
            this.year.Margin = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.year.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.year.Minimum = new decimal(new int[] {
            1964,
            0,
            0,
            0});
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(50, 20);
            this.year.TabIndex = 5;
            this.year.Value = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            // 
            // rdataLabel
            // 
            this.rdataLabel.AutoSize = true;
            this.rdataLabel.Location = new System.Drawing.Point(460, 30);
            this.rdataLabel.Name = "rdataLabel";
            this.rdataLabel.Size = new System.Drawing.Size(101, 13);
            this.rdataLabel.TabIndex = 13;
            this.rdataLabel.Text = "Расчетный период";
            // 
            // ost1Check
            // 
            this.ost1Check.AutoSize = true;
            this.ost1Check.Checked = true;
            this.ost1Check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ost1Check.Location = new System.Drawing.Point(683, 29);
            this.ost1Check.Name = "ost1Check";
            this.ost1Check.Size = new System.Drawing.Size(120, 17);
            this.ost1Check.TabIndex = 6;
            this.ost1Check.Text = "Коррек.ост.на н/м";
            this.ost1Check.UseVisualStyleBackColor = true;
            this.ost1Check.CheckedChanged += new System.EventHandler(this.ost1Check_CheckedChanged);
            // 
            // mainDataGrid
            // 
            this.mainDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGrid.ContextMenuStrip = this.MainTableContextMenu;
            this.mainDataGrid.Location = new System.Drawing.Point(0, 54);
            this.mainDataGrid.MultiSelect = false;
            this.mainDataGrid.Name = "mainDataGrid";
            this.mainDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mainDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.mainDataGrid.Size = new System.Drawing.Size(1084, 431);
            this.mainDataGrid.TabIndex = 0;
            this.mainDataGrid.DataSourceChanged += new System.EventHandler(this.MainDataGrid_DataSourceChanged);
            this.mainDataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainDataGrid_CellEndEdit);
            this.mainDataGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MainDataGrid_EditingControlShowing);
            // 
            // searchText
            // 
            this.searchText.Location = new System.Drawing.Point(3, 16);
            this.searchText.Name = "searchText";
            this.searchText.Size = new System.Drawing.Size(190, 20);
            this.searchText.TabIndex = 7;
            this.searchText.TextChanged += new System.EventHandler(this.searchText_TextChanged);
            this.searchText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchText_KeyPress);
            // 
            // searchBox
            // 
            this.searchBox.Controls.Add(this.searchText);
            this.searchBox.Location = new System.Drawing.Point(0, 53);
            this.searchBox.Name = "searchBox";
            this.searchBox.Padding = new System.Windows.Forms.Padding(1);
            this.searchBox.Size = new System.Drawing.Size(196, 39);
            this.searchBox.TabIndex = 7;
            this.searchBox.TabStop = false;
            this.searchBox.Text = "Поиск по номеру карточки";
            this.searchBox.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 485);
            this.Controls.Add(this.mainDataGrid);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.ost1Check);
            this.Controls.Add(this.rdataLabel);
            this.Controls.Add(this.year);
            this.Controls.Add(this.month);
            this.Controls.Add(this.kcexk);
            this.Controls.Add(this.naimkLabel);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.kkladLabel);
            this.Controls.Add(this.naimk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Баланс";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainTableContextMenu.ResumeLayout(false);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.month)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).EndInit();
            this.searchBox.ResumeLayout(false);
            this.searchBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обработкаЗаписиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обработкаИнформацииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетToolStripMenuItem;
        private System.Windows.Forms.ComboBox kcexk;
        private System.Windows.Forms.Label naimkLabel;
        private System.Windows.Forms.Label kkladLabel;
        private System.Windows.Forms.ComboBox naimk;
        private System.Windows.Forms.NumericUpDown month;
        private System.Windows.Forms.NumericUpDown year;
        private System.Windows.Forms.Label rdataLabel;
        private System.Windows.Forms.ContextMenuStrip MainTableContextMenu;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникЕдиницИзмеренияToolStripMenuItem;
        private System.Windows.Forms.CheckBox ost1Check;
        private System.Windows.Forms.ToolStripMenuItem обновитьДанныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объединитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поискToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объединениеКарточекToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обнудениеЦеныприToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поискToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem обнулениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem минимальнаяЗарплатаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem инвентаризацияToolStripMenuItem;
        private System.Windows.Forms.DataGridView mainDataGrid;
        private System.Windows.Forms.TextBox searchText;
        private System.Windows.Forms.GroupBox searchBox;
        private System.Windows.Forms.ToolStripMenuItem переносОстатковНаНачалоМесяцаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переходНаНовыйМесяцToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеКарточекСНулевымОстаткомНа1ЯнваряToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перенумерацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem начатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завершитьССохранниемИзмененийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завершитьБезСохраненияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обороткаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem подготовкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шапкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переносРасходовОбороткиВПриходнойБалансToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчётПоБалансуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem остаткиПоБалансуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пустографкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникКладовыхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрольToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem просчетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пустографкаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tESTReserveCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem очиститьВсеПолякромеСчетаИToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sssToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкаОтображенияСтолбцовToolStripMenuItem;
    }
}

