﻿using Balans.DataSets;
using Balans.Forms;
using Balans.Forms.Params;
using Balans.Reports;
using Balans.Structs;
using ColumnsPropertiesNamespace;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Balans
{
    public partial class MainForm : BaseForm
    {
        string selectedNaimk = "";
        decimal selectedKcexk;
        Oborotka oborotka;

        public MainForm()
        {
            //Splash.ShowSplashScreen();
            InitializeComponent();
            month.Value = DateTime.Now.Month;
            year.Value = DateTime.Now.Year;           
        }

        private void MainForm_Load(object sender, EventArgs e)
        {   
            LoadProperties();

            mainDataGrid.RowHeadersWidth = 40;
            mainDataGrid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;

            UpdateListKladData();

            UpdateUnitsData();

            AddColumnsToTable();

            UpdateKcexkList();

            ost1Check.Checked = ost1Value;

            this.mainDataGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MainDataGrid_CellFormatting);
            this.ost1Check.CheckedChanged += new System.EventHandler(this.ost1Check_CheckedChanged);

            this.TopMost = true;
            this.TopMost = false;
            this.Activate();
        }

        override public void PropertiesInitialization()
        {
            columnsProperties = new ColumnsProperties();

            columnsProperties.Add("NCARDS", "Номер карты");
            columnsProperties.Add("SIGN", "Обозначение");
            columnsProperties.Add("NAIM", "Наименование");
            columnsProperties.Add("CENA", "Цена");
            columnsProperties.Add("OST1", "Остаток на начало");
            columnsProperties.Add("OST2", "Остаток на конец");
            columnsProperties.Add("PRIH", "Приход");
            columnsProperties.Add("units", "Единица измерения");
            columnsProperties.Add("CHET", "Счет");
            columnsProperties.Add("RASH1", "Расход 1");
            columnsProperties.Add("RASH2", "Расход 2");
            columnsProperties.Add("RASH3", "Расход 3");
            columnsProperties.Add("RASH4", "Расход 4");
            columnsProperties.Add("RASH5", "Расход 5");
            columnsProperties.Add("RASH6", "Расход 6");
            columnsProperties.Add("RASH7", "Расход 7");
            columnsProperties.Add("RASH8", "Расход 8");
            columnsProperties.Add("RASH9", "Расход 9");
            columnsProperties.Add("RASH10", "Расход 10");
            columnsProperties.Add("BRAK", "Брак");
            columnsProperties.Add("SUM", "Сумма");
        }           
        private void AddUnitsColumn()
        {
            DataGridViewComboBoxColumn cbColumn = new DataGridViewComboBoxColumn { Name = "units" };
            cbColumn.DataPropertyName = "KEI";
            UNITS.UNITSRow rowUNITSRow = (UNITS.UNITSRow)unitsTable.NewRow();
            rowUNITSRow.KEI = "";
            rowUNITSRow.NAIM = " ";
            List<UNITS.UNITSRow> rows = unitsTable.ToList();
            rows.Add(rowUNITSRow);
            cbColumn.DataSource = rows.OrderBy(u => u.KEI).ToList();
            cbColumn.ValueMember = "KEI";
            cbColumn.DisplayMember = "KEI";

            mainDataGrid.Columns.Add(cbColumn);
        }
        private void AddColumnsToTable()
        {
            AddUnitsColumn();

            DataGridViewColumn renumberingColumn = new DataGridViewTextBoxColumn { Name = "Renumber" };
            renumberingColumn.DefaultCellStyle.NullValue = null;
            mainDataGrid.Columns.Add(renumberingColumn);
            mainDataGrid.Columns["Renumber"].Visible = false;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveProperties();
            Application.Exit();
        }

        /// <summary>
        /// обновление содержимого ComboBox с номерами цехов(и запускаем цепочку обновлений)
        /// </summary>
        public void UpdateKcexkList()
        {
            kcexk.Items.Clear();
            List<object> list = new List<object>();
            IEnumerable<LISTKLAD.LISTKLADRow> listrows = listKladTable.OrderBy(l => l.KCEXK);
            foreach (LISTKLAD.LISTKLADRow row in listrows)
            {
                list.Add(row.KCEXK);
            }
            kcexk.Items.AddRange(list.Distinct().ToArray<object>());
            if (kcexk.Items.Contains(selectedKcexk))
            {
                kcexk.SelectedItem = selectedKcexk;
            }
            else
            {
                kcexk.SelectedIndex = 0;
            }
        }

        private void Kcexk_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedKcexk = Convert.ToDecimal(kcexk.SelectedItem);
            UpdateNaimkList();
        }

        /// <summary>
        /// обновление содержимого ComboBox с наименованиями кладовых
        /// </summary>
        public void UpdateNaimkList()
        {
            naimk.Items.Clear();            
            IEnumerable<LISTKLAD.LISTKLADRow> listrows = listKladTable.OrderBy(l => l.NAIMK);
            foreach (LISTKLAD.LISTKLADRow row in listrows)
            {
                if (row.KCEXK.ToString() == (kcexk.SelectedItem.ToString()))
                {
                    naimk.Items.Add(row.NAIMK);
                }
            }            
            if (naimk.Items.Contains(selectedNaimk))
            {
                naimk.SelectedItem = selectedNaimk;
            }
            else
            {
                naimk.SelectedIndex = 0;
            }
        }

        private void Naimk_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mainDataGrid.Columns["Renumber"]!=null && mainDataGrid.Columns["Renumber"].Visible == true)
            {
                renumberingValues = new List<Renumbering>();
            }
            selectedNaimk = naimk.SelectedItem.ToString();
            _currentKlad = listKladTable.Where(lk => lk.NAIMK.Equals(naimk.SelectedItem) && lk.KCEXK.Equals(kcexk.SelectedItem)).First();
            year.Value = _currentKlad.RDATE.Year;
            month.Value = _currentKlad.RDATE.Month;
            SetTitleText();            
            CloseOborotka();
            ResetDataSource();
        }

        /// <summary>
        /// обновление данных balansmsp
        /// </summary>
        public void UpdateBalansData()
        {
            balansmspTable = balansmsp.GetDataByListKlad(_idListKlad);            
        }
        /// <summary>
        /// обновление данных listklad
        /// </summary>
        public void UpdateListKladData()
        {           
            listKladTable = listKlad.GetDataByKtype(1);            

            if (listKladTable == null || listKladTable.Count() < 1)
            {
                MessageBox.Show("Не удалось загрузить данные", "Нет данных", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Application.Exit();
            }
        }
        /// <summary>
        /// обновление данных units
        /// </summary>
        public void UpdateUnitsData()
        {
            unitsTable = units.GetData();
        }

        /// <summary>
        /// обновление данных в таблице по идентификаторам кладовок
        /// </summary>
        private void ResetDataSource()
        {
            UpdateBalansData();    
           
            mainDataGrid.DataSource = _balansDataRowsList;

            SetColumnsIndex();
        }

        private void SetTitleText()
        {
            Text = "Баланс \"" + selectedNaimk.Trim(' ') + "\" по " + selectedKcexk + " цеху за " + month.Value + '.' + year.Value;
        }

        /// <summary>
        /// добавление новой записи
        /// </summary>
        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewRec();
        }
        /// <summary>
        /// добавление новой записи
        /// </summary>
        private void AddNewRec()
        {
            if (MessageBox.Show("Вы уверены что хотите добавить новую запись?", "Добавление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.DataSource = null;

                AddUnitsColumn();

                int ID = Convert.ToInt32(balansmsp.NewID());

                balansmsp.Update(balansmspTable.AddBALANSMSPRow(ID, Convert.ToDecimal(_idListKlad), 0, " ", " ", 0, 0, null, 0, 0, 0, DateTime.Now, "1", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

                ResetDataSource();

                FocusOnLastAdded(ID);                
            }
        }
        /// <summary>
        /// копирование выделеной записи
        /// </summary>
        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneRec();
        }
        /// <summary>
        /// копирование выделеной записи
        /// </summary>
        private void CloneRec()
        {
            if (mainDataGrid.SelectedCells.Count > 0)
            {
                int FirstDisplayedRow = mainDataGrid.FirstDisplayedScrollingRowIndex;

                BALANSMSP.BALANSMSPRow kartRow;

                int ID = Convert.ToInt32(balansmsp.NewID());
                
                kartRow = (BALANSMSP.BALANSMSPRow)mainDataGrid.SelectedCells[0].OwningRow.DataBoundItem;

                balansmsp.Update(balansmspTable.AddBALANSMSPRow(ID, kartRow.ID_LISTKLAD, kartRow.NCARDS, kartRow.SIGN, kartRow.NAIM, 0, 0, kartRow.KEI, 0, 0, 0, DateTime.Now, kartRow.CHET, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

                ResetDataSource();

                FocusOnClone(Convert.ToDecimal(ID), FirstDisplayedRow);
            }
            else
            {
                MessageBox.Show("Запись для копирования не выбрана.", "Копирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRec();
        }

        private void DeleteRec()
        {
            if (MessageBox.Show("Вы уверены что хотите удалить запись?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {                
                if (mainDataGrid.SelectedCells.Count > 0)
                {
                    int FirstDisplayedRow = mainDataGrid.FirstDisplayedScrollingRowIndex;
                    int rowIndex = mainDataGrid.SelectedCells[0].RowIndex;
                    BALANSMSP.BALANSMSPRow row = (BALANSMSP.BALANSMSPRow)mainDataGrid.SelectedCells[0].OwningRow.DataBoundItem;

                    mainDataGrid.DataSource = null;

                    AddUnitsColumn();

                    row.Delete();
                    balansmsp.Update(row);
                    ResetDataSource();
                    FocusOnRow(rowIndex > 0 ? rowIndex - 1 : 0, FirstDisplayedRow);
                }            
                else
                {
                    MessageBox.Show("Для удаления выберите запись", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                }               
            }            
        }

        private void удалениеКарточекСНулевымОстаткомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRecWithZeroOst();
        }

        private void DeleteRecWithZeroOst()
        {
            if (MessageBox.Show("Вы уверены что хотите удалить записи с остатком равным 0?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                mainDataGrid.DataSource = null;

                AddUnitsColumn();

                List<BALANSMSP.BALANSMSPRow> forDelete = new List<BALANSMSP.BALANSMSPRow>();
                foreach (BALANSMSP.BALANSMSPRow row in _balansDataRowsList)
                {
                    if (row.OST1 == 0)
                    {
                        forDelete.Add(row);
                    }
                }
                foreach (var row in forDelete)
                {
                    row.Delete();
                    balansmsp.Update(row);
                }
                ResetDataSource();
            }
        }
        /// <summary>
        /// Объединение записей с одинаковой ценой, цехом и номером карточки
        /// </summary>
        private void объединитьToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            CombineRows();
        }
        /// <summary>
        /// объединение строк с одинаковыми цехом, картой, ценой
        /// </summary>
        private void CombineRows()
        {
            if (MessageBox.Show("Вы уверены, что хотите объеденить карточки?", "Объединение", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());

                if (wn.Count() > 0)
                {
                    ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                    msg.Show();
                }
                else
                {
                    mainDataGrid.DataSource = null;
                    AddUnitsColumn();

                    List<BALANSMSP.BALANSMSPRow> forDelete = new List<BALANSMSP.BALANSMSPRow>();
                    BALANSMSP.BALANSMSPRow bufRow;
                    BALANSMSP.BALANSMSPRow[] balansArray = _balansDataRowsList.OrderBy(k => k.CHET).ThenBy(k => k.NCARDS).ThenBy(k => k.NAIM).ThenBy(k => k.CENA).ToArray();

                    for (int index = 0; index < balansArray.Count() - 1; index++)
                    {
                        for (int secondIndex = index + 1; secondIndex < balansArray.Count(); secondIndex++)
                        {
                            if (balansArray[index].CHET.Equals(balansArray[secondIndex].CHET) && balansArray[index].NCARDS.Equals(balansArray[secondIndex].NCARDS) && balansArray[index].NAIM == balansArray[secondIndex].NAIM &&
                                balansArray[index].CENA.Equals(balansArray[secondIndex].CENA))
                            {
                                forDelete.Add(balansArray[secondIndex]);

                                bufRow = balansArray[index];

                                bufRow.OST1 = (bufRow.OST1 ?? 0) + (balansArray[secondIndex].OST1 ?? 0);
                                bufRow.PRIH = (bufRow.PRIH ?? 0) + (balansArray[secondIndex].PRIH ?? 0);
                                bufRow.RASH1 = (bufRow.RASH1 ?? 0) + (balansArray[secondIndex].RASH1 ?? 0);
                                bufRow.RASH2 = (bufRow.RASH2 ?? 0) + (balansArray[secondIndex].RASH2 ?? 0);
                                bufRow.RASH3 = (bufRow.RASH3 ?? 0) + (balansArray[secondIndex].RASH3 ?? 0);
                                bufRow.RASH4 = (bufRow.RASH4 ?? 0) + (balansArray[secondIndex].RASH4 ?? 0);
                                bufRow.RASH5 = (bufRow.RASH5 ?? 0) + (balansArray[secondIndex].RASH5 ?? 0);
                                bufRow.RASH6 = (bufRow.RASH6 ?? 0) + (balansArray[secondIndex].RASH6 ?? 0);
                                bufRow.RASH7 = (bufRow.RASH7 ?? 0) + (balansArray[secondIndex].RASH7 ?? 0);
                                bufRow.RASH8 = (bufRow.RASH8 ?? 0) + (balansArray[secondIndex].RASH8 ?? 0);
                                bufRow.RASH9 = (bufRow.RASH9 ?? 0) + (balansArray[secondIndex].RASH9 ?? 0);
                                bufRow.RASH10 = (bufRow.RASH10 ?? 0) + (balansArray[secondIndex].RASH10 ?? 0);
                                bufRow.BRAK = (bufRow.BRAK ?? 0) + (balansArray[secondIndex].BRAK ?? 0);

                                bufRow.DDAT = DateTime.Now;

                                bufRow.OST2 = (bufRow.OST1 ?? 0) + (bufRow.PRIH ?? 0) - (bufRow.RASH1 ?? 0) - (bufRow.RASH2 ?? 0) - (bufRow.RASH3 ?? 0) - (bufRow.RASH4 ?? 0) - (bufRow.RASH5 ?? 0) -
                                    (bufRow.RASH6 ?? 0) - (bufRow.RASH7 ?? 0) - (bufRow.RASH8 ?? 0) - (bufRow.RASH9 ?? 0) - (bufRow.RASH10 ?? 0) - (bufRow.BRAK ?? 0);

                                balansmsp.Update(bufRow);
                            }
                            else
                            {
                                index = secondIndex - 1;
                                break;
                            }
                            if (secondIndex == balansArray.Count() - 1)
                            {
                                index = secondIndex;
                            }
                        }
                    }

                    for (int index = 0; index < balansArray.Count() - 1; index++)
                    {
                        if (balansArray[index].CENA == 0 && balansArray[index].OST1 == 0 && balansArray[index].CHET.Equals(balansArray[index + 1].CHET) &&
                            balansArray[index].NCARDS.Equals(balansArray[index + 1].NCARDS) && balansArray[index].NAIM == balansArray[index + 1].NAIM)
                        {
                            if (!forDelete.Contains(balansArray[index + 1]))
                            {
                                forDelete.Add(balansArray[index]);
                            }
                        }
                    }

                    foreach (var row in forDelete.Distinct())
                    {
                        row.Delete();
                        balansmsp.Update(row);
                    }
                    ResetDataSource();
                }
            }
        }

        /// <summary>
        /// объединение строк с одинаковыми цехом, картой, ценой
        /// </summary>
        private void DeleteZeroDuplicatesOborotka()
        {
            KARTOTEKA.KARTOTEKARow[] kartotekaArray = kartoteka.GetDataByListklad((decimal)_idListKlad).OrderBy(k => k.NCARDS).ThenBy(k => k.NAIM).ThenBy(k => k.OST1).ThenBy(k => k.CENA).ToArray();

            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaArray);

            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                List<KARTOTEKA.KARTOTEKARow> forDeleteKartoteka = new List<KARTOTEKA.KARTOTEKARow>();
                
                for (int index = 0; index < kartotekaArray.Count() - 1; index++)
                {
                    if (kartotekaArray[index].CENA == 0 && kartotekaArray[index].OST1 == 0 && kartotekaArray[index].CHET.Equals(kartotekaArray[index + 1].CHET) &&
                        kartotekaArray[index].NCARDS.Equals(kartotekaArray[index + 1].NCARDS) && kartotekaArray[index].NAIM == kartotekaArray[index + 1].NAIM)
                    {
                        forDeleteKartoteka.Add(kartotekaArray[index]);
                    }
                }

                foreach (var row in forDeleteKartoteka)
                {
                    row.Delete();
                    kartoteka.Update(row);
                }
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Общая информация", "О программе");
        }

        /// <summary>
        /// Отображает меню поиска
        /// </summary>
        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenSearchPanel();
        }

        /// <summary>
        /// открывает панель поиска
        /// </summary>
        private void OpenSearchPanel()
        {
            if (поискToolStripMenuItem.Checked)
            {
                поискToolStripMenuItem.Checked = false;
                поискToolStripMenuItem1.Checked = false;
                mainDataGrid.Location = new Point(mainDataGrid.Location.X, mainDataGrid.Location.Y - searchBox.Height);
                mainDataGrid.Height += searchBox.Height;
                searchBox.Visible = false;
                searchText.Text = "";
                ResetDataSource();
            }
            else
            {
                поискToolStripMenuItem.Checked = true;
                поискToolStripMenuItem1.Checked = true;
                searchBox.Visible = true;
                searchText.Focus();
                mainDataGrid.Location = new Point(mainDataGrid.Location.X, mainDataGrid.Location.Y + searchBox.Height);
                mainDataGrid.Height -= searchBox.Height;
            }
        }

        /// <summary>
        /// поиск по введенному тексту
        /// </summary>
        private void searchText_TextChanged(object sender, EventArgs e)
        {
            FocusOnSearchRow();
        }

        private void объединениеКарточекToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CombineRows();
        }

        private void поискToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenSearchPanel();
        }

        private void копированиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneRec();
        }

        private void вставкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewRec();
        }

        private void удалениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRec();
        }

        private void обнулениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetZeroPrice();
        }

        private void обнулениеЦеныприToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetZeroPrice();
        }

        private void справочникЕдиницИзмеренияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnitsForm unitsForm = new UnitsForm();
            unitsForm.Show();
        }

        private void RecountAll()
        {
            foreach (var row in _balansDataRowsList)
            {
                Recount(row);
            }
            ResetDataSource();
        }
        private void Recount(BALANSMSP.BALANSMSPRow row)
        {
            decimal ost2 = (row.OST1 ?? 0) + (row.PRIH ?? 0) - (row.RASH1 ?? 0) - (row.RASH2 ?? 0) - (row.RASH3 ?? 0) - (row.RASH4 ?? 0) - (row.RASH5 ?? 0) 
                - (row.RASH6 ?? 0) - (row.RASH7 ?? 0) - (row.RASH8 ?? 0) - (row.RASH9 ?? 0) - (row.RASH10 ?? 0) - (row.BRAK ?? 0);
            row.OST2 = ost2;
            row.SUM = (row.OST2 ?? 0) * (row.CENA ?? 0);
            balansmsp.Update(row);
        }

        private void ost1Check_CheckedChanged(object sender, EventArgs e)
        {
            if (ost1Check.Checked)
            {
                ost1Value = true;
            }
            else
            {
                ost1Value = false;
            }
            mainDataGrid.Columns["OST1"].ReadOnly = !ost1Value;
            mainDataGrid.Refresh();
        }

        /// <summary>
        /// событие привязки данных
        /// </summary>
        private void MainDataGrid_DataSourceChanged(object sender, EventArgs e)
        {
            if(mainDataGrid.DataSource != null)
            {
                foreach(var col in columnsProperties.Columns)
                {
                    mainDataGrid.Columns[col.ColumnName].HeaderText = col.DisplayName;
                }

                mainDataGrid.Columns["Renumber"].HeaderText = "Перенумерация";
                mainDataGrid.Columns["ID"].Visible = false;
                mainDataGrid.Columns["ID_LISTKLAD"].Visible = false;
                mainDataGrid.Columns["RowError"].Visible = false;
                mainDataGrid.Columns["RowState"].Visible = false;
                mainDataGrid.Columns["Table"].Visible = false;
                mainDataGrid.Columns["HasErrors"].Visible = false;
                mainDataGrid.Columns["CENAS"].Visible = false;
                mainDataGrid.Columns["DDAT"].Visible = false;

                SetColumnsWidth();

                mainDataGrid.Columns["OST1"].ReadOnly = !ost1Value;
                mainDataGrid.Columns["OST2"].ReadOnly = true;
                mainDataGrid.Columns["SUM"].ReadOnly = true;
            }
        }
        private void SetColumnsWidth()
        {
            this.mainDataGrid.ColumnWidthChanged -= new System.Windows.Forms.DataGridViewColumnEventHandler(this.MainDataGrid_ColumnWidthChanged);

            foreach (var prop in columnsProperties.Columns)
            {
                if (prop.ColumnWidth != null)
                {
                    mainDataGrid.Columns[prop.ColumnName].Width = (int)prop.ColumnWidth;
                }
            }

            this.mainDataGrid.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.MainDataGrid_ColumnWidthChanged);
        }
        private void SetColumnsIndex()
        {            
            columnsProperties.ResetIndices();
            foreach (var prop in columnsProperties.Columns)
            {
                if (prop.DisplayIndex != null)
                {
                    mainDataGrid.Columns[prop.ColumnName].DisplayIndex = (int)prop.DisplayIndex;
                }
            }
        }


        private void MainDataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = mainDataGrid.CurrentCell.ColumnIndex;


            if (columnIndex == mainDataGrid.Columns["NCARDS"].Index || columnIndex == mainDataGrid.Columns["CHET"].Index || columnIndex == mainDataGrid.Columns["Renumber"].Index)
            {
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress += new KeyPressEventHandler(KeyPressEventInt_DataTable);
            }
            else
            if (columnIndex == mainDataGrid.Columns["PRIH"].Index || columnIndex == mainDataGrid.Columns["OST1"].Index || columnIndex == mainDataGrid.Columns["OST2"].Index
                || columnIndex == mainDataGrid.Columns["CENA"].Index || columnIndex == mainDataGrid.Columns["RASH1"].Index || columnIndex == mainDataGrid.Columns["RASH2"].Index
                || columnIndex == mainDataGrid.Columns["RASH3"].Index || columnIndex == mainDataGrid.Columns["RASH4"].Index || columnIndex == mainDataGrid.Columns["RASH5"].Index
                || columnIndex == mainDataGrid.Columns["RASH6"].Index || columnIndex == mainDataGrid.Columns["RASH7"].Index || columnIndex == mainDataGrid.Columns["RASH8"].Index
                || columnIndex == mainDataGrid.Columns["RASH9"].Index || columnIndex == mainDataGrid.Columns["RASH10"].Index || columnIndex == mainDataGrid.Columns["BRAK"].Index)
            {
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
                e.Control.KeyPress += new KeyPressEventHandler(KeyPressEventFloat_DataTable);
            }
            else
            {
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
            }

        }

        private void переходНаНовыйМесяцToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetZeroPrice();
            CombineRows();
        }

        private void начатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renumberingValues = new List<Renumbering>();
            mainDataGrid.Columns["Renumber"].Visible = true;
            mainDataGrid.Columns["Renumber"].DisplayIndex = mainDataGrid.Columns["NCARDS"].DisplayIndex + 1;
            fillRenumberingColumnNullValues();
        }

        private void fillRenumberingColumnNullValues()
        {
            foreach(DataGridViewRow item in mainDataGrid.Rows)
            {
                item.Cells["Renumber"].Value = null ;
            }
        }

        private void завершитьССохранниемИзмененийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите сохранить изменения?", "Завершение перенумерации", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.EndEdit();
                SaveRenumbering();
                mainDataGrid.Columns["Renumber"].Visible = false;
                renumberingValues.Clear();
                ResetDataSource();
            }
        }

        private void SaveRenumbering()
        {
            if (renumberingValues.Count > 0)
            {
                for (int i = 0; i < mainDataGrid.RowCount; i++)
                {
                    if (mainDataGrid["Renumber", i].Value!= null)
                    {
                        mainDataGrid["NCARDS", i].Value = Convert.ToDecimal(mainDataGrid["Renumber", i].Value);
                    }
                }
            }
        }

        private void завершитьБезСохраненияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите завершить операцию без сохранения?", "Завершение перенумерации", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.Columns["Renumber"].Visible = false;
                renumberingValues.Clear();
            }
        }

        private void mainDataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && mainDataGrid.Rows[e.RowIndex].Visible)
            {
                var row = (BALANSMSP.BALANSMSPRow)mainDataGrid.Rows[e.RowIndex].DataBoundItem;
                if (e.ColumnIndex == mainDataGrid.Columns["Renumber"].Index)
                {
                    if (!String.IsNullOrEmpty(mainDataGrid["Renumber", e.RowIndex].Value.ToString()))
                    {
                        renumberingValues.Add(new Renumbering { ID = (int)row.ID, newNCards = Convert.ToInt32(mainDataGrid["Renumber", e.RowIndex].Value) });
                    }
                    else
                    {
                        var item = renumberingValues.Find(r => r.ID == row.ID);
                        renumberingValues.Remove(item);
                    }
                }
                if (new string[] {"CENA", "PRIH", "OST1", "RASH1", "RASH2", "RASH3", "RASH4", "RASH5", "RASH6", "RASH7", "RASH8", "RASH9", "RASH10", "BRAK"}
                .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
                {
                    Recount(row);
                    mainDataGrid.Refresh();
                }
                row.DDAT = DateTime.Now;
                balansmsp.Update(row);
                if (new string[] { "CENA", "NAIM", "SIGN", "NCARDS" }
               .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        ResortAfterEdit();
                    }));
                }
            }
        }

        //числа с дробной частью
        private void KeyPressEventFloat_DataTable(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        //числа без дробной части
        private void KeyPressEventInt_DataTable(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void шапкаToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            RashNames rashNames = new RashNames(Convert.ToDecimal(_idListKlad));
            rashNames.Show();
        }

        private void обороткаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOborotka();
            oborotka = new Oborotka(_currentKlad, month.Value, year.Value);
            oborotka.Show();            
        }

        public void CloseOborotka()
        {
            if (Oborotka.isExist)
            {
                oborotka.Close();
            }
        }

        private void переносРасходовОбороткиВПриходнойБалансToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TransferOborotkaToBalans();
        }

        /// <summary>
        /// перенос расходов оборотки в баланс
        /// </summary>
        private void TransferOborotkaToBalans()
        {
            if (MessageBox.Show("Вы уверены, что хотите перенести расходы оборотки в приходной баланс?", "Перенос", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.DataSource = null;

                AddUnitsColumn();

                List<KARTOTEKA.KARTOTEKARow> kartotekaRowsList = kartoteka.GetForTransfer((decimal)_idListKlad).OrderBy(k => k.CHET).ThenBy(k => k.NCARDS).ThenBy(k => k.NAIM).ThenBy(k => k.SIGN).ThenBy(k => k.CENA).ToList();                

                int ID = 0;
                decimal _cenas, _cena, _ost2, _sum;
                string _chet;

                for (int index = 0; index < kartotekaRowsList.Count; index++)
                {
                    var kartRow = kartotekaRowsList[index];
                    decimal _rash = kartRow.RASH ?? 0;
                    if (index <= kartotekaRowsList.Count - 1)
                    {
                        for (int secondIndex = index + 1; secondIndex < kartotekaRowsList.Count; secondIndex++)
                        {
                            var secondKartRow = kartotekaRowsList[secondIndex];

                            if (kartRow.NCARDS == secondKartRow.NCARDS && kartRow.NAIM == secondKartRow.NAIM && kartRow.SIGN == secondKartRow.SIGN && kartRow.CENA == secondKartRow.CENA)
                            {
                                _rash += secondKartRow.RASH ?? 0;
                            }
                            else
                            {
                                index = secondIndex - 1;
                                break;
                            }
                            if (secondIndex == kartotekaRowsList.Count() - 1)
                            {
                                index = secondIndex;
                            }
                        }
                    }
                    var balansRows = _balansDataRowsList.Where(b => b.NAIM == kartRow.NAIM && b.SIGN == kartRow.SIGN && b.NCARDS == kartRow.NCARDS && b.CENA == kartRow.CENA).ToList();
                    if (balansRows.Count() > 0)
                    {
                        balansRows[0].PRIH = _rash;
                        Recount(balansRows[0]);
                        balansmsp.Update(balansRows[0]);
                    }
                    else
                    {
                        ID = Convert.ToInt32(balansmsp.NewID());
                        _cenas = kartRow.CENAS ?? 0;
                        _cena = kartRow.CENA ?? 0;
                        _chet = kartRow.CHET;
                        _ost2 = kartRow.RASH ?? 0;
                        _sum = _cena * _ost2;
                        balansmsp.Update(balansmspTable.AddBALANSMSPRow(ID, Convert.ToDecimal(_idListKlad), kartRow.NCARDS, kartRow.SIGN, kartRow.NAIM, _cenas, _cena, kartRow.KEI, 0, _rash, _ost2, DateTime.Now, _chet, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, _sum));
                    }
                }
                ResetDataSource();
                RecountAll();
            }
        }

        private void searchText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void минимальнаяЗарплатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MinSalary salaryForm = new MinSalary(seting);
            salaryForm.ShowDialog();
        }
      

        private void пустографкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_balansDataRowsList.Count() > 0)
                {
                    PustografkaRdlc rb = new PustografkaRdlc(_balansDataRowsList, Convert.ToInt32(kcexk.SelectedItem), naimk.SelectedItem.ToString(), month.Value, year.Value);
                }
                else
                {
                    MessageBox.Show("В базе данных баланса отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void остаткиПоБалансуToolStripMenuItem_Click(object sender, EventArgs e)
        {         
            IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_balansDataRowsList.Count() > 0)
                {
                    RemainMaterialsBalansRdlc rb = new RemainMaterialsBalansRdlc(_balansDataRowsList, Convert.ToInt32(kcexk.SelectedItem), naimk.SelectedItem.ToString(), month.Value, year.Value);
                }
                else
                {
                    MessageBox.Show("В базе данных баланса отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveProperties();
            this.Close();
        }

        private void отчётПоБалансуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());

            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_balansDataRowsList.Count() > 0)
                {
                    BalansReportRdlc rb = new BalansReportRdlc(_balansDataRowsList, Convert.ToInt32(kcexk.SelectedItem), naimk.SelectedItem.ToString(), month.Value, year.Value);
                }
                else
                {
                    MessageBox.Show("В базе данных баланса отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void инвентаризацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {               
                if (_balansDataRowsList.Count() > 0)
                {
                    InventoryBalansReportRdlc rb = new InventoryBalansReportRdlc(_balansDataRowsList, Convert.ToInt32(kcexk.SelectedItem), naimk.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("В базе данных баланса отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void справочникКладовыхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListKladForm unitsForm = new ListKladForm(this);
            unitsForm.Show();
        }

        private void контрольToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(balansmspTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {               
                if (_balansDataRowsList.Count() > 0)
                {
                    ControlRdlc rb = new ControlRdlc(_balansDataRowsList, Convert.ToInt32(kcexk.SelectedItem), naimk.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("В базе данных баланса отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void просчетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите пересчитать остаток на конец месяца во всех записях?", "Пересчет", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                RecountAll();
            }
        }

        private void пустографкаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Pustografki();
        }
        /// <summary>
        /// Перенос остатков с концца месяца на начало и очистка всех приходов и расходов
        /// </summary>
        private void Pustografki()
        {
            using (ConfirmForm confirmForm = new ConfirmForm("пустографки", "Перенос остатков на начало месяца.\nДанные о приходах и расходах будут удалены. "))
            {
                confirmForm.ShowDialog();
                if (confirmForm.DialogResult == DialogResult.OK)
                {
                    CloseOborotka();
                    foreach (var item in _balansDataRowsList)
                    {
                        item.OST1 = item.OST2;
                        item.PRIH = 0;
                        item.RASH1 = 0;
                        item.RASH2 = 0;
                        item.RASH3 = 0;
                        item.RASH4 = 0;
                        item.RASH5 = 0;
                        item.RASH6 = 0;
                        item.RASH7 = 0;
                        item.RASH8 = 0;
                        item.RASH9 = 0;
                        item.RASH10 = 0;
                        item.BRAK = 0;
                        balansmsp.Update(item);
                    }

                    kartotekaTable = kartoteka.GetDataByListklad((decimal)_idListKlad);
                    foreach (var item in kartotekaTable)
                    {
                        item.OST1 = item.OST2;
                        item.PRIH = 0;
                        item.RASH = 0;
                        item.RASHC = 0;
                        kartoteka.Update(item);
                    }
                    CombineRows();
                }
            }
        }

        private void переносОстатковНаНачалоМесяцаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoToNewMonth();
        }   

        private void GoToNewMonth()
        {//перенос остатков на новый месяц
            if (MessageBox.Show("Вы уверены что хотите перенести остатки на начало месяца?\nДанные о приходах и расходах будут удалены.", "Перенос остатков", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                foreach (var item in _balansDataRowsList)
                {
                    item.OST1 = item.OST2;
                    item.PRIH = 0;
                    item.RASH1 = 0;
                    item.RASH2 = 0;
                    item.RASH3 = 0;
                    item.RASH4 = 0;
                    item.RASH5 = 0;
                    item.RASH6 = 0;
                    item.RASH7 = 0;
                    item.RASH8 = 0;
                    item.RASH9 = 0;
                    item.RASH10 = 0;
                    item.BRAK = 0;

                    balansmsp.Update(item);
                }
                ResetDataSource();
            }
        }

        //окрашивание ячеек
        private void MainDataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (Convert.ToDecimal(mainDataGrid["OST2", e.RowIndex].Value) < 0)
            {
                e.CellStyle.BackColor = Color.IndianRed;
            }
            else if (e.ColumnIndex == mainDataGrid.Columns["PRIH"].Index)
            {
                e.CellStyle.BackColor = Color.LightSkyBlue;
            }
            else
            {
                if (e.RowIndex % 2 == 0 && e.ColumnIndex != mainDataGrid.Columns["units"].Index)
                {
                    e.CellStyle.BackColor = Color.Bisque;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["OST1"].Index && !ost1Check.Checked)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["OST2"].Index || e.ColumnIndex == mainDataGrid.Columns["NCARDS"].Index)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["Renumber"].Index)
                {
                    e.CellStyle.BackColor = Color.BurlyWood;
                }
            }
            if (new string[] { "CENA", "PRIH", "OST1", "OST2", "RASH1", "RASH2", "RASH3", "RASH4", "RASH5", "RASH6", "RASH7", "RASH8", "RASH9", "RASH10", "BRAK", "NCARDS" }
                .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
            {
                e.CellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            }
        }

        /// <summary>
        /// изменение размеров столбцов
        /// </summary>
        private void MainDataGrid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Column column = columnsProperties.FindByName(e.Column.Name);
            if (column != null)
            {
                column.ColumnWidth = e.Column.Width;
            }
        }

        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = mainDataGrid.SelectedCells.Count > 0 ? mainDataGrid.SelectedCells[0].RowIndex : 0;
            int firstDisplayed = mainDataGrid.FirstDisplayedScrollingRowIndex;
            UpdateBalansData();
            UpdateKcexkList();
            //выделяем обратно строку
            FocusOnRow(rowIndex, firstDisplayed);
        }
        /// <summary>
        /// показывает пользователю на экране последний добавленный элемент
        /// </summary>
        private void FocusOnLastAdded(int ID)
        {
            for (int i = 0; i < mainDataGrid.Rows.Count; i++)
            {
                if (mainDataGrid["ID", i].Value.Equals(ID))                {

                    FocusOnRow(i, i);
                    break;
                }
            }
        }

        private void FocusOnClone(decimal ID, int firstDisplayed)
        {
            for (int i = 0; i < mainDataGrid.Rows.Count; i++)
            {
                if (mainDataGrid["ID", i].Value.Equals(ID))
                {
                    FocusOnRow(i, firstDisplayed);
                    break;
                }
            }
        }


        private void FocusOnSearchRow()
        {
            if (decimal.TryParse(searchText.Text, out decimal buf))
            {
                for (int i = 0; i < mainDataGrid.Rows.Count; i++)
                {
                    if (mainDataGrid.Rows[i].Cells["NCARDS"].Value.Equals(buf))
                    {
                        FocusOnRow(i, i);
                        break;
                    }
                }
            }
            else
            {
                searchText.Text = "";
            }
        }

        private void FocusOnRow(int focusedRow, int firstDisplayedRow)
        {
            if (focusedRow > 0 && mainDataGrid.Rows.Count >= focusedRow)
            {
                mainDataGrid.Rows[focusedRow].Cells["NCARDS"].Selected = true;
                mainDataGrid.FirstDisplayedScrollingRowIndex = firstDisplayedRow;
            }
        }

        /// <summary>
        /// устанавливает цену в 0 при остатке равном 0
        /// </summary>
        private void SetZeroPrice()
        {
            if (MessageBox.Show("Вы уверены что хотите обнулить цену при остатке равном нулю во всех записях?", "Обнуление цены", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                foreach (BALANSMSP.BALANSMSPRow row in _balansDataRowsList)
                {
                    if (Convert.ToDecimal(row.OST1) == 0)
                    {
                        row.CENA = 0;
                        row.PRIH = 0;
                        row.RASH1 = 0;
                        row.RASH2 = 0;
                        row.RASH3 = 0;
                        row.RASH4 = 0;
                        row.RASH5 = 0;
                        row.RASH6 = 0;
                        row.RASH7 = 0;
                        row.RASH8 = 0;
                        row.RASH9 = 0;
                        row.RASH10 = 0;
                        row.BRAK = 0;

                        row.DDAT = DateTime.Now;

                        balansmsp.Update(row);
                    }
                }
                ResetDataSource();
            }
        }       

        private void TESTReserveCopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите создать резервную копию? \n Предыдущие копии этой кладовой будут уничтожены!", "Резервное копирование", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                decimal idlistklad;
                List<BALANSMSP.BALANSMSPRow> backupBalans = balansmsp.GetDataByListKlad(_idListKlad).ToList();

                List<KARTOTEKA.KARTOTEKARow> backupKartoteka = kartoteka.GetData().Where(k => k.ID_LISTKLAD.Equals(_idListKlad)).ToList();

                List<LISTKLAD.LISTKLADRow> listkladList = listKlad.GetDataByKtype(3).Where(l => l.NAIMK == _idListKlad.ToString()).ToList();
                if (listkladList.Count > 0)
                {
                    listkladList[0].Delete();
                    listKlad.Update(listkladList[0]);
                }

                idlistklad = (decimal)listKlad.NewID();                
                listKlad.Update(listKladTable.AddLISTKLADRow(idlistklad, 0, 0, _idListKlad.ToString(), 0, 3, DateTime.Now, DateTime.Now));

                foreach (var row in backupBalans)
                {
                    balansmsp.Update(balansmspTable.AddBALANSMSPRow((decimal)balansmsp.NewID(), idlistklad, row.NCARDS, row.SIGN, row.NAIM, row.CENAS ?? 0, row.CENA ?? 0, row.KEI,
                        row.OST1 ?? 0, row.PRIH ?? 0, row.OST2 ?? 0, row.DDAT, row.CHET, row.RASH1 ?? 0, row.RASH2 ?? 0, row.RASH3 ?? 0, row.RASH4 ?? 0, row.RASH5 ?? 0, row.RASH6 ?? 0, row.RASH7 ?? 0, row.RASH8 ?? 0,
                        row.RASH9 ?? 0, row.RASH10 ?? 0, row.BRAK ?? 0, row.SUM ?? 0));
                }
                foreach (var row in backupKartoteka)
                {
                    kartoteka.Update(kartotekaTable.AddKARTOTEKARow((decimal)kartoteka.NewID(), idlistklad, row.NCARDS, row.NN ?? 0, row.SIGN, row.NAIM, row.CENAS ?? 0, row.CENA ?? 0, row.KEI,
                        row.OST1 ?? 0, row.PRIH ?? 0, row.RASH ?? 0, row.OST2 ?? 0, row.DDAT, row.CHET, row.RASHC ?? 0, row.INDUSTRY, row.SUM ?? 0));
                }                
            }
        }

        private void ClearAllFieldsEvent(object sender, EventArgs e)
        {
            using (ConfirmForm confirmForm = new ConfirmForm("подтверждаю","Подтверждение полной очистки"))
            {
                confirmForm.ShowDialog();
                if (confirmForm.DialogResult == DialogResult.OK)
                {                    
                    ClearAllFields();
                }
            }
        }

        private void ClearAllFields()
        {
            mainDataGrid.DataSource = null;
            AddUnitsColumn();

            foreach (var row in _balansDataRowsList)
            {
                row.CENA = 0;
                row.CENAS = 0;
                row.PRIH = 0;
                row.RASH1 = 0;
                row.RASH2 = 0;
                row.RASH3 = 0;
                row.RASH4 = 0;
                row.RASH5 = 0;
                row.RASH6 = 0;
                row.RASH7 = 0;
                row.RASH8 = 0;
                row.RASH9 = 0;
                row.RASH10 = 0;
                row.BRAK = 0;
                row.NAIM = "";
                row.SIGN = "";
                row.OST1 = 0;
                row.OST2 = 0;                

                row.DDAT = DateTime.Now;

                balansmsp.Update(row);
            }
            ResetDataSource();
        }
        protected override IniFile GetSettingsFile()
        {
            Directory.CreateDirectory("C:\\Users\\Public\\properties_MSP");
            return new IniFile("C:\\Users\\Public\\properties_MSP\\balans.ini");
        }
        private void ResortAfterEdit()
        {
            var id = mainDataGrid.SelectedCells[0].OwningRow.Cells["ID"].Value;
            var columnName = mainDataGrid.SelectedCells[0].OwningColumn.Name;
            var rowInd = mainDataGrid.FirstDisplayedScrollingRowIndex;

            //UpdateBalansData();
            
            mainDataGrid.DataSource = _balansDataRowsList;

            SetColumnsIndex();

            foreach (DataGridViewRow gridRow in mainDataGrid.Rows)
            {
                if (gridRow.Cells["ID"].Value.Equals(id))
                {
                    gridRow.Cells[columnName].Selected = true;
                    mainDataGrid.FirstDisplayedScrollingRowIndex = rowInd;
                    break;
                }
            }
        }

        private void настройкаОтображенияСтолбцовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColumnsOrder co = new ColumnsOrder(columnsProperties);
            co.ShowDialog();
            SetColumnsIndex();
        }
    }
}