﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Balans.DataSets.RASHNAMETableAdapters;
using Balans.DataSets;

namespace Balans.Forms
{
    public partial class RashNames : Form
    {
        RASHNAMETableAdapter rashNameAdapter;
        public RashNames(decimal idListklad)
        {
            InitializeComponent();
            rashNameAdapter = new RASHNAMETableAdapter();
            List<RASHNAME.RASHNAMERow> rashNameList = rashNameAdapter.GetDataByLISTKLAD(idListklad).OrderBy(r => r.ID).ToList();

            if(rashNameList == null || rashNameList.Count() < 10)
            {
                int countRows = rashNameList.Count();
                for (int i = 0; i < 10 - countRows; i++)
                {
                    rashNameAdapter.Insert(Convert.ToDecimal(rashNameAdapter.NewID()), idListklad, "", "", "");
                }
                rashNameList = rashNameAdapter.GetDataByLISTKLAD(idListklad).OrderBy(r => r.ID).ToList();
            }
            
            rashNamesGrid.DataSource = rashNameList;
         
            this.rashNamesGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.rashNamesGrid_CellValueChanged);
        }

        private void rashNamesGrid_DataSourceChanged(object sender, EventArgs e)
        {
            rashNamesGrid.Columns["ID"].Visible = false;
            rashNamesGrid.Columns["ID_LISTKLAD"].Visible = false;
            rashNamesGrid.Columns["RowError"].Visible = false;
            rashNamesGrid.Columns["RowState"].Visible = false;
            rashNamesGrid.Columns["Table"].Visible = false;
            rashNamesGrid.Columns["HasErrors"].Visible = false;
            rashNamesGrid.Columns["NAME1"].HeaderText = "строка 1";
            rashNamesGrid.Columns["NAME2"].HeaderText = "строка 2";
            rashNamesGrid.Columns["NAME3"].HeaderText = "строка 3";
        }

        private void rashNamesGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            rashNameAdapter.Update((RASHNAME.RASHNAMERow)rashNamesGrid.Rows[e.RowIndex].DataBoundItem);
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAllFields();
        }

        private void ClearAllFields()
        {
            foreach (DataGridViewRow row in rashNamesGrid.Rows)
            {
                RASHNAME.RASHNAMERow rrow = (RASHNAME.RASHNAMERow)row.DataBoundItem;
                rrow.SetNAME1Null();
                rrow.SetNAME2Null();
                rrow.SetNAME3Null();
                rashNameAdapter.Update(rrow);
            }
            rashNamesGrid.Refresh();
        }

        private void rashNamesGrid_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            e.Row.HeaderCell.Value = (e.Row.Index + 1).ToString();
        }
    }
}
