﻿using Microsoft.Reporting.WinForms;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

namespace Balans
{
    public partial class PrintReport : Form
    {
        private readonly ReportViewer _reportViewer;

        public PrintReport()
        {
            InitializeComponent();
        }

        private PrintReport(object report)
            : this()
        {
            _reportViewer = report as ReportViewer;
            if (_reportViewer != null)
            {
                SettingReport();
                DisplayReport();
            }
        }

        private PrintReport(DataTable dataSource, string sourceName)
            : this()
        {
            if (dataSource != null && sourceName != null)
            {
                _reportViewer = new ReportViewer();
                _reportViewer.LocalReport.DataSources.Add(new ReportDataSource(sourceName, dataSource));
                SettingReport();
                DisplayReport();
            }
        }

        public static void ShowReport(Component report)
        {
            if (report as ReportViewer != null)
            {
                var formViewReport = new PrintReport(report);
                formViewReport.Show();
            }
        }

        public static void ShowReport(DataTable dataSource, string sourceName)
        {
            using (var formViewReport = new PrintReport(dataSource, sourceName))
            {
                formViewReport.ShowDialog();
            }
        }

        private void SettingReport()
        {
            _reportViewer.Dock = DockStyle.Fill;
            _reportViewer.SetDisplayMode(DisplayMode.PrintLayout);
            _reportViewer.ZoomMode = ZoomMode.FullPage;

            System.Drawing.Printing.PageSettings ps = new System.Drawing.Printing.PageSettings();
            ps.Margins.Bottom = 30;
            ps.Margins.Top = 30;
            ps.Margins.Right = 1;
            ps.Margins.Left = 10;
            ps.Landscape = false;
            ps.PaperSize = new System.Drawing.Printing.PaperSize("A4", 1338, 1063);
            _reportViewer.SetPageSettings(ps);

            RenameUnwantedExportFormat(_reportViewer, "EXCELOPENXML", "Excel 2007-2010");
            EnableUnwantedExportFormat(_reportViewer, "Excel");
        }

        private void DisplayReport()
        {
            WindowState = FormWindowState.Maximized;
            Controls.Add(_reportViewer);

            _reportViewer.RefreshReport();
        }

        private void EnableUnwantedExportFormat(ReportViewer reportViewerId, string strFormatName)
        {
            foreach (RenderingExtension extension in reportViewerId.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    FieldInfo info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    if (info != null) info.SetValue(extension, true);
                }
            }
        }

        private void RenameUnwantedExportFormat(ReportViewer reportViewerId, string strFormatName, string newName)
        {
            foreach (RenderingExtension extension in reportViewerId.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    FieldInfo info = extension.GetType().GetField("m_localizedName",
                        BindingFlags.Instance | BindingFlags.NonPublic);
                    if (info != null) info.SetValue(extension, newName);
                }
            }
        }
    }
}
