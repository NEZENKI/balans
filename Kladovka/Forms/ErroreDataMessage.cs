﻿using Balans.Structs;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Balans.Forms
{
    public partial class ErroreDataMessage : Form
    {
        IEnumerable<IncorrectValue> _errData;

        public ErroreDataMessage(IEnumerable<IncorrectValue> errData, string title, string message)
        {
            InitializeComponent();
            _errData = errData;
            this.message.Text = message;
            this.Text = title;
        }   
    
        private void Message_Load(object sender, EventArgs e)
        {
            foreach (IncorrectValue item in _errData)
            {
                mainDataGrid.Rows.Add(item.chet, item.nkards);
            }
        }
    }
}
