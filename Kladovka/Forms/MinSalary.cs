﻿using System;
using System.Windows.Forms;

namespace Balans
{
    public partial class MinSalary : Form
    {
        private IniFile iniProp;
        public MinSalary(IniFile prop)
        {
            iniProp = prop;
            InitializeComponent();
        }

        private void MinSalary_Load(object sender, EventArgs e)
        {
            if (iniProp.ReadINI("SALARY", "SALARY").Length > 0)
            {
                salaryBox.Text = iniProp.ReadINI("SALARY", "SALARY").ToString();
            }
            else
            {
                salaryBox.Text = "Введите данные для сохранения";
            }
            if (iniProp.ReadINI("SALARY", "DATA").Length > 0)
            {
                minSalaryData.Value = Convert.ToDateTime(iniProp.ReadINI("SALARY", "DATA"));
            }
            else
            {
                minSalaryData.Value = DateTime.Now;
            }
        }

        private void MinSalary_FormClosing(object sender, FormClosingEventArgs e)
        {
            iniProp.Write("SALARY", "DATA", minSalaryData.Value.ToString());
            iniProp.Write("SALARY", "SALARY", salaryBox.Text.ToString());
        }
    }
}
