﻿namespace Balans.Forms
{
    partial class ErroreDataMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.message = new System.Windows.Forms.Label();
            this.mainDataGrid = new System.Windows.Forms.DataGridView();
            this.Chet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NCARDS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Location = new System.Drawing.Point(12, 9);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(202, 13);
            this.message.TabIndex = 0;
            this.message.Text = "Разные наименования при значениях:";
            // 
            // mainDataGrid
            // 
            this.mainDataGrid.AllowUserToAddRows = false;
            this.mainDataGrid.AllowUserToDeleteRows = false;
            this.mainDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mainDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Chet,
            this.NCARDS});
            this.mainDataGrid.Location = new System.Drawing.Point(1, 25);
            this.mainDataGrid.Margin = new System.Windows.Forms.Padding(0);
            this.mainDataGrid.Name = "mainDataGrid";
            this.mainDataGrid.ReadOnly = true;
            this.mainDataGrid.RowHeadersVisible = false;
            this.mainDataGrid.Size = new System.Drawing.Size(367, 118);
            this.mainDataGrid.TabIndex = 1;
            // 
            // Chet
            // 
            this.Chet.HeaderText = "Счет";
            this.Chet.Name = "Chet";
            this.Chet.ReadOnly = true;
            // 
            // NCARDS
            // 
            this.NCARDS.HeaderText = "Номер карты";
            this.NCARDS.Name = "NCARDS";
            this.NCARDS.ReadOnly = true;
            // 
            // ErroreDataMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 143);
            this.Controls.Add(this.mainDataGrid);
            this.Controls.Add(this.message);
            this.Name = "ErroreDataMessage";
            this.Text = "Ошибка наименования";
            this.Load += new System.EventHandler(this.Message_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label message;
        private System.Windows.Forms.DataGridView mainDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chet;
        private System.Windows.Forms.DataGridViewTextBoxColumn NCARDS;
    }
}