﻿using ColumnsPropertiesNamespace;
using Balans.DataSets;
using Balans.DataSets.BALANSMSPTableAdapters;
using Balans.DataSets.INDUSTRIESTableAdapters;
using Balans.DataSets.KARTOTEKATableAdapters;
using Balans.DataSets.LISTKLADTableAdapters;
using Balans.DataSets.UNITSTableAdapters;
using Balans.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Balans.Forms
{
    public partial class BaseForm : Form
    {
        protected decimal _idListKlad { get => _currentKlad.ID; }

        protected KARTOTEKATableAdapter kartoteka = new KARTOTEKATableAdapter();
        protected KARTOTEKA.KARTOTEKADataTable kartotekaTable;
        /// <summary>
        /// отсортированный список карточек оборотки
        /// </summary>
        protected List<KARTOTEKA.KARTOTEKARow> _kartotekaDataRowsList { get => kartotekaTable.OrderBy(k => k.CHET).ThenBy(k => k.NCARDS).ThenBy(k => k.NAIM).ThenBy(k => k.SIGN).ThenBy(k => k.CENA).ToList(); }

        protected INDUSTRIESTableAdapter industries = new INDUSTRIESTableAdapter();
        protected INDUSTRIES.INDUSTRIESDataTable industriesTable;

        protected BALANSMSPTableAdapter balansmsp = new BALANSMSPTableAdapter();
        protected BALANSMSP.BALANSMSPDataTable balansmspTable;
        /// <summary>
        /// отсортированный список карточек баланса
        /// </summary>
        protected List<BALANSMSP.BALANSMSPRow> _balansDataRowsList { get => balansmspTable.OrderBy(k => k.CHET).ThenBy(k => k.NCARDS).ThenBy(k => k.NAIM).ThenBy(k => k.SIGN).ThenBy(k => k.CENA).ToList(); }

        protected LISTKLADTableAdapter listKlad = new LISTKLADTableAdapter();
        protected LISTKLAD.LISTKLADDataTable listKladTable;
        protected LISTKLAD.LISTKLADRow _currentKlad;

        protected UNITSTableAdapter units = new UNITSTableAdapter();
        protected UNITS.UNITSDataTable unitsTable;

        protected ColumnsProperties columnsProperties;

        protected IniFile seting;

        protected List<Renumbering> renumberingValues;

        protected bool ost1Value;

        public BaseForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// заполняем список столбцов для натсройки
        /// </summary>
        virtual public void PropertiesInitialization()
        {
        }
        /// <summary>
        /// получаем файл настроек
        /// </summary>
        /// <returns>файл, хранящий настройки</returns>
        virtual protected IniFile GetSettingsFile()
        {
            return null;
        }
        /// <summary>
        /// загрузка размеров столбцов
        /// </summary>
        protected void LoadProperties()
        {
            seting = GetSettingsFile();           
            PropertiesInitialization();

            foreach (var prop in columnsProperties.Columns)
            {
                if (seting.ReadINI("INDEX", prop.ColumnName).Length > 0)
                {
                    prop.DisplayIndex = Convert.ToInt32(seting.ReadINI("INDEX", prop.ColumnName));
                }
                prop.ColumnWidth = seting.ReadINI("WIDTH", prop.ColumnName).Length > 0 ? Convert.ToInt32(seting.ReadINI("WIDTH", prop.ColumnName)) : 60;
            }

            foreach (var prop in columnsProperties.Columns.Where(c => c.DisplayIndex == null))
            {
                prop.DisplayIndex = columnsProperties.GetNewIndex();
            }

            ost1Value = seting.ReadINI("CHECK", "OST1").Length > 0 ? Convert.ToBoolean(seting.ReadINI("CHECK", "OST1").ToLower()) : true;

            if (seting.ReadINI("WINDOW", "WIDTH").Length > 0 && Convert.ToDecimal(seting.ReadINI("WINDOW", "WIDTH")) > 500)
            {
                this.Width = Convert.ToInt32(seting.ReadINI("WINDOW", "WIDTH"));
            }
            if (seting.ReadINI("WINDOW", "HEIGHT").Length > 0 && Convert.ToDecimal(seting.ReadINI("WINDOW", "HEIGHT")) > 200)
            {
                this.Height = Convert.ToInt32(seting.ReadINI("WINDOW", "HEIGHT"));
            }
        }
        /// <summary>
        /// сохранение размеров и положеия столбцов
        /// </summary>
        virtual protected void SaveProperties()
        {
            if (columnsProperties != null)
            {
                foreach (var prop in columnsProperties.Columns)
                {
                    seting.Write("WIDTH", prop.ColumnName, prop.ColumnWidth.ToString());
                    seting.Write("INDEX", prop.ColumnName, prop.DisplayIndex.ToString());
                }

                seting.Write("check", "OST1", ost1Value.ToString());

                seting.Write("WINDOW", "WIDTH", Width.ToString());
                seting.Write("WINDOW", "HEIGHT", Height.ToString());
            }
        }

        /// <summary>
        /// проверка ошибок наименоваия в одинаковых карточках баланса
        /// </summary>
        /// <param name="balansArray">список карточек с ошибками наименования</param>
        /// <returns></returns>
        protected IEnumerable<IncorrectValue> NamesValidation(BALANSMSP.BALANSMSPRow[] balansArray)
        {
            List<IncorrectValue> incorrectNaim = new List<IncorrectValue>();

            for (int index = 0; index < balansArray.Count(); index++)
            {
                for (int secondIndex = index + 1; secondIndex < balansArray.Count(); secondIndex++)
                {
                    if (balansArray[index].CHET.Equals(balansArray[secondIndex].CHET) && balansArray[index].NCARDS.Equals(balansArray[secondIndex].NCARDS)
                        && ((balansArray[index].NAIM != balansArray[secondIndex].NAIM) || (balansArray[index].SIGN != balansArray[secondIndex].SIGN)))
                    {
                        incorrectNaim.Add(new IncorrectValue() { chet = balansArray[secondIndex].CHET, nkards = balansArray[secondIndex].NCARDS });
                        index = secondIndex;
                    }
                    else
                    {
                        index = secondIndex - 1;
                        break;
                    }
                }
            }
            return incorrectNaim.Distinct();
        }
        /// <summary>
        /// проверка ошибок наименоваия в одинаковых карточках оборотки
        /// </summary>
        /// <param name="balansArray">список карточек с ошибками наименования</param>
        /// <returns></returns>
        protected IEnumerable<IncorrectValue> NamesValidation(KARTOTEKA.KARTOTEKARow[] kartotekaArray)
        {
            List<IncorrectValue> incorrectNaim = new List<IncorrectValue>();

            for (int index = 0; index < kartotekaArray.Count(); index++)
            {
                if (!incorrectNaim.Contains(new IncorrectValue() { chet = kartotekaArray[index].CHET, nkards = kartotekaArray[index].ID_LISTKLAD }))
                {
                    for (int secondIndex = index + 1; secondIndex < kartotekaArray.Count(); secondIndex++)
                    {
                        if (kartotekaArray[index].CHET.Equals(kartotekaArray[secondIndex].CHET) && kartotekaArray[index].NCARDS.Equals(kartotekaArray[secondIndex].NCARDS)
                            && ((kartotekaArray[index].NAIM != kartotekaArray[secondIndex].NAIM) || (kartotekaArray[index].SIGN != kartotekaArray[secondIndex].SIGN)))
                        {
                            incorrectNaim.Add(new IncorrectValue() { chet = kartotekaArray[secondIndex].CHET, nkards = kartotekaArray[secondIndex].NCARDS });
                            index = secondIndex;
                        }
                        else
                        {
                            index = secondIndex - 1;
                            break;
                        }
                    }
                }
            }
            return incorrectNaim.Distinct();
        }               
    }
}
