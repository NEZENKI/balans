﻿using Balans.DataSets;
using Balans.DataSets.INDUSTRIESTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Balans
{
    public partial class IndustriesForm : Form
    {
        INDUSTRIESTableAdapter industries = new INDUSTRIESTableAdapter();
        INDUSTRIES industriesTable = new INDUSTRIES();
        IEnumerable<INDUSTRIES.INDUSTRIESRow> indRowsList;

        public IndustriesForm()
        {
            InitializeComponent();
        }

        private void Units_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "iNDUSTRIES1._INDUSTRIES". При необходимости она может быть перемещена или удалена.
            industries.Fill(industriesTable._INDUSTRIES);
            updateTable();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            updateTable();
        }
        public void updateTable()
        {
            industriesData.DataSource = indRowsList = industriesTable._INDUSTRIES.Where(u => u.NAME.ToLower().Contains(SearchBox.Text.ToLower())).OrderBy(u => u.NAME).ToList();            
        }

        private void unitsData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            industries.Update(indRowsList.ToList()[e.RowIndex]);
        }

        private void unitsData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            Color secondColor = Color.Bisque;
            if (e.RowIndex % 2 == 0)
            {
                e.CellStyle.BackColor = secondColor;
            }
        }

        private void unitsData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == industriesData.Columns["NAME"].Index)
            {
                if (e.FormattedValue.ToString().Trim(' ').Length > 20)
                {
                    industriesData.Rows[e.RowIndex].ErrorText = "Строка вышла за границы";
                    e.Cancel = true;
                }
            }
        }

        private void unitsData_DataSourceChanged(object sender, EventArgs e)
        {
            industriesData.RowHeadersVisible = false;
            industriesData.Columns["RowError"].Visible = false;
            industriesData.Columns["RowState"].Visible = false;
            industriesData.Columns["ID"].Visible = false;
            industriesData.Columns["Table"].Visible = false;
            industriesData.Columns["HasErrors"].Visible = false;
            industriesData.Columns["NAME"].HeaderText = "Наименование";
            industriesData.Columns["SHORTNAME"].HeaderText = "Краткое имя";
            industriesData.Columns["CODE"].HeaderText = "Код";
            industriesData.Columns["NAME"].Width = 50 * industriesData.Width / 100;
            industriesData.Columns["SHORTNAME"].Width = 25 * industriesData.Width / 100;
            industriesData.Columns["CODE"].Width = 25 * industriesData.Width / 100;
        }

        private void Industries_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.industriesData.EndEdit();
        }
    }
}
