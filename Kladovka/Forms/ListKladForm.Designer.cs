﻿namespace Balans
{
    partial class ListKladForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListKladForm));
            this.listKladData = new System.Windows.Forms.DataGridView();
            this.SearchBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.listKladData)).BeginInit();
            this.SuspendLayout();
            // 
            // listKladData
            // 
            this.listKladData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listKladData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.listKladData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listKladData.Location = new System.Drawing.Point(0, 20);
            this.listKladData.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.listKladData.Name = "listKladData";
            this.listKladData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.listKladData.Size = new System.Drawing.Size(242, 364);
            this.listKladData.TabIndex = 0;
            this.listKladData.DataSourceChanged += new System.EventHandler(this.unitsData_DataSourceChanged);
            this.listKladData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.unitsData_CellEndEdit);
            this.listKladData.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.unitsData_CellFormatting);
            this.listKladData.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.unitsData_CellValidating);
            // 
            // SearchBox
            // 
            this.SearchBox.Location = new System.Drawing.Point(0, 0);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(242, 20);
            this.SearchBox.TabIndex = 1;
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // ListKladForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 385);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.listKladData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListKladForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Справочник кладовых";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ListKladForm_FormClosing);
            this.Load += new System.EventHandler(this.Units_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listKladData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView listKladData;
        private System.Windows.Forms.TextBox SearchBox;
    }
}