﻿namespace Balans
{
    partial class UnitsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnitsForm));
            this.unitsData = new System.Windows.Forms.DataGridView();
            this.SearchBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.unitsData)).BeginInit();
            this.SuspendLayout();
            // 
            // unitsData
            // 
            this.unitsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitsData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.unitsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.unitsData.Location = new System.Drawing.Point(0, 20);
            this.unitsData.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.unitsData.Name = "unitsData";
            this.unitsData.RowHeadersVisible = false;
            this.unitsData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.unitsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.unitsData.Size = new System.Drawing.Size(242, 364);
            this.unitsData.TabIndex = 0;
            this.unitsData.DataSourceChanged += new System.EventHandler(this.unitsData_DataSourceChanged);
            this.unitsData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.unitsData_CellEndEdit);
            this.unitsData.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.unitsData_CellFormatting);
            this.unitsData.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.unitsData_CellValidating);
            // 
            // SearchBox
            // 
            this.SearchBox.Location = new System.Drawing.Point(0, 0);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(242, 20);
            this.SearchBox.TabIndex = 1;
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // UnitsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 385);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.unitsData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnitsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Справочник единиц измерения";
            this.Load += new System.EventHandler(this.Units_Load);
            ((System.ComponentModel.ISupportInitialize)(this.unitsData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView unitsData;
        private System.Windows.Forms.TextBox SearchBox;
    }
}