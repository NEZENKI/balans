﻿namespace Balans
{
    partial class Oborotka
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Oborotka));
            this.MainTableContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объединитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обнулениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перенумерацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.начатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьССохранниемИзмененийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.завершитьБезСохраненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обновитьДанныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обработкаЗаписиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.копированиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объединениеКарточекToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обнудениеЦеныприToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетПоОбороткеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.остаткиПоОбороткеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.остаткиПоОтрослямToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.минимальнаяЗарплатаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникЕдиницИзмеренияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникОтрослейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ost1Check = new System.Windows.Forms.CheckBox();
            this.mainDataGrid = new System.Windows.Forms.DataGridView();
            this.searchText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.настройкаОтображенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainTableContextMenu.SuspendLayout();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // MainTableContextMenu
            // 
            this.MainTableContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem,
            this.добавитьToolStripMenuItem,
            this.копироватьToolStripMenuItem,
            this.объединитьToolStripMenuItem,
            this.обнулениеToolStripMenuItem,
            this.перенумерацияToolStripMenuItem,
            this.настройкаОтображенияToolStripMenuItem});
            this.MainTableContextMenu.Name = "MainTableContextMenu";
            this.MainTableContextMenu.Size = new System.Drawing.Size(266, 180);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.удалитьToolStripMenuItem.Text = "Удаление";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.добавитьToolStripMenuItem.Text = "Вставка";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.добавитьToolStripMenuItem_Click);
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.копироватьToolStripMenuItem.Text = "Копирование";
            this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.копироватьToolStripMenuItem_Click);
            // 
            // объединитьToolStripMenuItem
            // 
            this.объединитьToolStripMenuItem.Name = "объединитьToolStripMenuItem";
            this.объединитьToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.объединитьToolStripMenuItem.Text = "Объединение карточек";
            this.объединитьToolStripMenuItem.Click += new System.EventHandler(this.объединитьToolStripMenuItem_Click);
            // 
            // обнулениеToolStripMenuItem
            // 
            this.обнулениеToolStripMenuItem.Name = "обнулениеToolStripMenuItem";
            this.обнулениеToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.обнулениеToolStripMenuItem.Text = "Обнуление";
            this.обнулениеToolStripMenuItem.Click += new System.EventHandler(this.обнулениеToolStripMenuItem_Click);
            // 
            // перенумерацияToolStripMenuItem
            // 
            this.перенумерацияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.начатьToolStripMenuItem,
            this.завершитьССохранниемИзмененийToolStripMenuItem,
            this.завершитьБезСохраненияToolStripMenuItem});
            this.перенумерацияToolStripMenuItem.Name = "перенумерацияToolStripMenuItem";
            this.перенумерацияToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.перенумерацияToolStripMenuItem.Text = "Перенумерация";
            // 
            // начатьToolStripMenuItem
            // 
            this.начатьToolStripMenuItem.Name = "начатьToolStripMenuItem";
            this.начатьToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.начатьToolStripMenuItem.Text = "Начать";
            this.начатьToolStripMenuItem.Click += new System.EventHandler(this.начатьToolStripMenuItem_Click);
            // 
            // завершитьССохранниемИзмененийToolStripMenuItem
            // 
            this.завершитьССохранниемИзмененийToolStripMenuItem.Name = "завершитьССохранниемИзмененийToolStripMenuItem";
            this.завершитьССохранниемИзмененийToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.завершитьССохранниемИзмененийToolStripMenuItem.Text = "Завершить с сохраннием";
            this.завершитьССохранниемИзмененийToolStripMenuItem.Click += new System.EventHandler(this.завершитьССохранниемИзмененийToolStripMenuItem_Click);
            // 
            // завершитьБезСохраненияToolStripMenuItem
            // 
            this.завершитьБезСохраненияToolStripMenuItem.Name = "завершитьБезСохраненияToolStripMenuItem";
            this.завершитьБезСохраненияToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.завершитьБезСохраненияToolStripMenuItem.Text = "Завершить без сохранения";
            this.завершитьБезСохраненияToolStripMenuItem.Click += new System.EventHandler(this.завершитьБезСохраненияToolStripMenuItem_Click);
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.Silver;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.обработкаЗаписиToolStripMenuItem,
            this.отчетToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1084, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menu";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьДанныеToolStripMenuItem,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // обновитьДанныеToolStripMenuItem
            // 
            this.обновитьДанныеToolStripMenuItem.Name = "обновитьДанныеToolStripMenuItem";
            this.обновитьДанныеToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.обновитьДанныеToolStripMenuItem.Text = "Обновить данные";
            this.обновитьДанныеToolStripMenuItem.Click += new System.EventHandler(this.обновитьДанныеToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // обработкаЗаписиToolStripMenuItem
            // 
            this.обработкаЗаписиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалениеToolStripMenuItem,
            this.вставкаToolStripMenuItem,
            this.копированиеToolStripMenuItem,
            this.объединениеКарточекToolStripMenuItem,
            this.обнудениеЦеныприToolStripMenuItem});
            this.обработкаЗаписиToolStripMenuItem.Name = "обработкаЗаписиToolStripMenuItem";
            this.обработкаЗаписиToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.обработкаЗаписиToolStripMenuItem.Text = "Обработка записи";
            // 
            // удалениеToolStripMenuItem
            // 
            this.удалениеToolStripMenuItem.Name = "удалениеToolStripMenuItem";
            this.удалениеToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.удалениеToolStripMenuItem.Text = "Удаление";
            this.удалениеToolStripMenuItem.Click += new System.EventHandler(this.удалениеToolStripMenuItem_Click);
            // 
            // вставкаToolStripMenuItem
            // 
            this.вставкаToolStripMenuItem.Name = "вставкаToolStripMenuItem";
            this.вставкаToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.вставкаToolStripMenuItem.Text = "Вставка";
            this.вставкаToolStripMenuItem.Click += new System.EventHandler(this.вставкаToolStripMenuItem_Click);
            // 
            // копированиеToolStripMenuItem
            // 
            this.копированиеToolStripMenuItem.Name = "копированиеToolStripMenuItem";
            this.копированиеToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.копированиеToolStripMenuItem.Text = "Копирование";
            this.копированиеToolStripMenuItem.Click += new System.EventHandler(this.копированиеToolStripMenuItem_Click);
            // 
            // объединениеКарточекToolStripMenuItem
            // 
            this.объединениеКарточекToolStripMenuItem.Name = "объединениеКарточекToolStripMenuItem";
            this.объединениеКарточекToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.объединениеКарточекToolStripMenuItem.Text = "Объединение карточек";
            this.объединениеКарточекToolStripMenuItem.Click += new System.EventHandler(this.объединениеКарточекToolStripMenuItem_Click);
            // 
            // обнудениеЦеныприToolStripMenuItem
            // 
            this.обнудениеЦеныприToolStripMenuItem.Name = "обнудениеЦеныприToolStripMenuItem";
            this.обнудениеЦеныприToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.обнудениеЦеныприToolStripMenuItem.Text = "Обнуление цены(при ост=0)";
            this.обнудениеЦеныприToolStripMenuItem.Click += new System.EventHandler(this.обнулениеЦеныприToolStripMenuItem_Click);
            // 
            // отчетToolStripMenuItem
            // 
            this.отчетToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетПоОбороткеToolStripMenuItem,
            this.остаткиПоОбороткеToolStripMenuItem,
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1,
            this.остаткиПоОтрослямToolStripMenuItem});
            this.отчетToolStripMenuItem.Name = "отчетToolStripMenuItem";
            this.отчетToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.отчетToolStripMenuItem.Text = "Отчет";
            // 
            // отчетПоОбороткеToolStripMenuItem
            // 
            this.отчетПоОбороткеToolStripMenuItem.Name = "отчетПоОбороткеToolStripMenuItem";
            this.отчетПоОбороткеToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.отчетПоОбороткеToolStripMenuItem.Text = "Отчет по оборотке";
            this.отчетПоОбороткеToolStripMenuItem.Click += new System.EventHandler(this.отчетПоОбороткеToolStripMenuItem_Click);
            // 
            // остаткиПоОбороткеToolStripMenuItem
            // 
            this.остаткиПоОбороткеToolStripMenuItem.Name = "остаткиПоОбороткеToolStripMenuItem";
            this.остаткиПоОбороткеToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.остаткиПоОбороткеToolStripMenuItem.Text = "Остатки по оборотке";
            this.остаткиПоОбороткеToolStripMenuItem.Click += new System.EventHandler(this.остаткиПоОбороткеToolStripMenuItem_Click);
            // 
            // отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1
            // 
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1.Name = "отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1";
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1.Size = new System.Drawing.Size(288, 22);
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1.Text = "Отчет по инвентаризации по оборотке";
            this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1.Click += new System.EventHandler(this.отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1_Click);
            // 
            // остаткиПоОтрослямToolStripMenuItem
            // 
            this.остаткиПоОтрослямToolStripMenuItem.Name = "остаткиПоОтрослямToolStripMenuItem";
            this.остаткиПоОтрослямToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.остаткиПоОтрослямToolStripMenuItem.Text = "Расходы по отрослям";
            this.остаткиПоОтрослямToolStripMenuItem.Click += new System.EventHandler(this.остаткиПоОтрослямToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.минимальнаяЗарплатаToolStripMenuItem,
            this.справочникЕдиницИзмеренияToolStripMenuItem,
            this.справочникОтрослейToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // минимальнаяЗарплатаToolStripMenuItem
            // 
            this.минимальнаяЗарплатаToolStripMenuItem.Name = "минимальнаяЗарплатаToolStripMenuItem";
            this.минимальнаяЗарплатаToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.минимальнаяЗарплатаToolStripMenuItem.Text = "Минимальная зарплата";
            this.минимальнаяЗарплатаToolStripMenuItem.Click += new System.EventHandler(this.минимальнаяЗарплатаToolStripMenuItem_Click);
            // 
            // справочникЕдиницИзмеренияToolStripMenuItem
            // 
            this.справочникЕдиницИзмеренияToolStripMenuItem.Name = "справочникЕдиницИзмеренияToolStripMenuItem";
            this.справочникЕдиницИзмеренияToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.справочникЕдиницИзмеренияToolStripMenuItem.Text = "Справочник единиц измерения";
            this.справочникЕдиницИзмеренияToolStripMenuItem.Click += new System.EventHandler(this.справочникЕдиницИзмеренияToolStripMenuItem_Click);
            // 
            // справочникОтрослейToolStripMenuItem
            // 
            this.справочникОтрослейToolStripMenuItem.Name = "справочникОтрослейToolStripMenuItem";
            this.справочникОтрослейToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.справочникОтрослейToolStripMenuItem.Text = "Справочник отраслей";
            this.справочникОтрослейToolStripMenuItem.Click += new System.EventHandler(this.справочникОтрослейToolStripMenuItem_Click);
            // 
            // ost1Check
            // 
            this.ost1Check.AutoSize = true;
            this.ost1Check.Checked = true;
            this.ost1Check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ost1Check.Location = new System.Drawing.Point(246, 31);
            this.ost1Check.Name = "ost1Check";
            this.ost1Check.Size = new System.Drawing.Size(120, 17);
            this.ost1Check.TabIndex = 6;
            this.ost1Check.Text = "Коррек.ост.на н/м";
            this.ost1Check.UseVisualStyleBackColor = true;
            this.ost1Check.CheckedChanged += new System.EventHandler(this.ost1Check_CheckedChanged);
            // 
            // mainDataGrid
            // 
            this.mainDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainDataGrid.ContextMenuStrip = this.MainTableContextMenu;
            this.mainDataGrid.Location = new System.Drawing.Point(0, 55);
            this.mainDataGrid.MultiSelect = false;
            this.mainDataGrid.Name = "mainDataGrid";
            this.mainDataGrid.RowHeadersWidth = 40;
            this.mainDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.mainDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.mainDataGrid.Size = new System.Drawing.Size(1084, 456);
            this.mainDataGrid.TabIndex = 0;
            this.mainDataGrid.DataSourceChanged += new System.EventHandler(this.MainDataGrid_DataSourceChanged);
            this.mainDataGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainDataGrid_CellEndEdit);
            this.mainDataGrid.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.MainDataGrid_EditingControlShowing);
            // 
            // searchText
            // 
            this.searchText.Location = new System.Drawing.Point(50, 29);
            this.searchText.Name = "searchText";
            this.searchText.Size = new System.Drawing.Size(190, 20);
            this.searchText.TabIndex = 7;
            this.searchText.TextChanged += new System.EventHandler(this.searchText_TextChanged);
            this.searchText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchText_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Поиск:";
            // 
            // настройкаОтображенияToolStripMenuItem
            // 
            this.настройкаОтображенияToolStripMenuItem.Name = "настройкаОтображенияToolStripMenuItem";
            this.настройкаОтображенияToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.настройкаОтображенияToolStripMenuItem.Text = "Настройка отображения столбцов";
            this.настройкаОтображенияToolStripMenuItem.Click += new System.EventHandler(this.настройкаОтображенияToolStripMenuItem_Click);
            // 
            // Oborotka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchText);
            this.Controls.Add(this.mainDataGrid);
            this.Controls.Add(this.ost1Check);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Oborotka";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оборотка";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Oborotka_FormClosed);
            this.Load += new System.EventHandler(this.Oborotka_Load);
            this.MainTableContextMenu.ResumeLayout(false);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обработкаЗаписиToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MainTableContextMenu;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникЕдиницИзмеренияToolStripMenuItem;
        private System.Windows.Forms.CheckBox ost1Check;
        private System.Windows.Forms.ToolStripMenuItem обновитьДанныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объединитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem копированиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объединениеКарточекToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обнудениеЦеныприToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обнулениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem минимальнаяЗарплатаToolStripMenuItem;
        private System.Windows.Forms.DataGridView mainDataGrid;
        private System.Windows.Forms.TextBox searchText;
        private System.Windows.Forms.ToolStripMenuItem перенумерацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem начатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завершитьССохранниемИзмененийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem завершитьБезСохраненияToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem справочникОтрослейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетПоОбороткеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem остаткиПоОбороткеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem остаткиПоОтрослямToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкаОтображенияToolStripMenuItem;
    }
}

