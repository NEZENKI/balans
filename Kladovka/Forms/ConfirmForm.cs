﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Balans.Forms
{
    public partial class ConfirmForm : Form
    {
        public ConfirmForm(string codeWord, string title)
        {
            InitializeComponent();
            Text = title;
            CodeWordTB.Text = codeWord;
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (CodeWordTB.Text.ToLower().Equals(ConfirmWordTB.Text.ToLower()))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void ConfirmWordTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (CodeWordTB.Text.ToLower().Equals(ConfirmWordTB.Text.ToLower()))
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }
    }
}
