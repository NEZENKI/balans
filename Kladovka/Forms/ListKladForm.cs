﻿using Balans.DataSets;
using Balans.DataSets.LISTKLADTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Balans
{
    public partial class ListKladForm : Form
    {
        LISTKLADTableAdapter listKlad = new LISTKLADTableAdapter();
        LISTKLAD.LISTKLADDataTable listKladTable = new LISTKLAD.LISTKLADDataTable();
        MainForm _mf;
        bool changed = false;

        public ListKladForm(MainForm mf)
        {
            InitializeComponent();
            _mf = mf;
        }

        private void Units_Load(object sender, EventArgs e)
        {
            listKladTable = listKlad.GetDataByKtype(1);
            updateTable();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            //updateTable();
        }
        public void updateTable()
        {
            listKladData.DataSource = listKladTable.OrderBy(l => l.KCEXK).ThenBy(l => l.NAIMK).ToList();            
        }

        private void unitsData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (listKlad.Update((LISTKLAD.LISTKLADRow)listKladData.Rows[e.RowIndex].DataBoundItem) > 0)
            {
                changed = true;
            }
        }

        private void unitsData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            Color secondColor = Color.Bisque;
            if (e.RowIndex % 2 == 0)
            {
                e.CellStyle.BackColor = secondColor;
            }
        }

        private void unitsData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == listKladData.Columns["NAIMK"].Index)
            {
                if (e.FormattedValue.ToString().Trim(' ').Length > 30)
                {
                    listKladData.Rows[e.RowIndex].ErrorText = "Строка вышла за границы";
                    e.Cancel = true;
                }
            }
        }

        private void unitsData_DataSourceChanged(object sender, EventArgs e)
        {
            listKladData.RowHeadersVisible = false;
            listKladData.Columns["RowError"].Visible = false;
            listKladData.Columns["RowState"].Visible = false;
            listKladData.Columns["Table"].Visible = false;
            listKladData.Columns["ID"].Visible = false;
            listKladData.Columns["ID_USERS"].Visible = false;
            listKladData.Columns["KKLAD"].Visible = false;
            listKladData.Columns["HasErrors"].Visible = false;
            listKladData.Columns["KTYPE"].Visible = false;
            listKladData.Columns["RDATE"].Visible = false;
            listKladData.Columns["RDATELAST"].Visible = false;
            listKladData.Columns["RDATE"].Visible = false;
            listKladData.Columns["RDATELAST"].Visible = false;
            listKladData.Columns["KCEXK"].HeaderText = "Код";
            listKladData.Columns["NAIMK"].HeaderText = "Наименование";
            listKladData.Columns["KCEXK"].Width = 30 * listKladData.Width / 100;
            listKladData.Columns["NAIMK"].Width = 70 * listKladData.Width / 100;
            listKladData.Columns["KCEXK"].ReadOnly = true;
        }

        private void ListKladForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            listKladData.EndEdit();
            if (changed)
            {
                _mf.UpdateListKladData();
                _mf.UpdateKcexkList();
            }
        }
    }
}
