﻿using Balans.DataSets;
using Balans.DataSets.UNITSTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Balans
{
    public partial class UnitsForm : Form
    {
        UNITSTableAdapter units = new UNITSTableAdapter();
        UNITS unitsTable = new UNITS();
        IEnumerable<UNITS.UNITSRow> unitsRowsList;

        public UnitsForm()
        {
            InitializeComponent();
        }

        private void Units_Load(object sender, EventArgs e)
        {
            units.Fill(unitsTable._UNITS);
            updateTable();
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            updateTable();
        }
        public void updateTable()
        {
            unitsData.DataSource = unitsRowsList = unitsTable._UNITS.Where(u => u.NAIM.Contains(SearchBox.Text)).OrderBy(u => u.KEI).ToList();            
        }

        private void unitsData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            units.Update(unitsRowsList.ToList()[e.RowIndex]);
        }

        private void unitsData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            Color secondColor = Color.Bisque;
            if (e.RowIndex % 2 == 0)
            {
                e.CellStyle.BackColor = secondColor;
            }
        }

        private void unitsData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == unitsData.Columns["NAIM"].Index)
            {
                if (e.FormattedValue.ToString().Trim(' ').Length > 20)
                {
                    unitsData.Rows[e.RowIndex].ErrorText = "Строка вышла за границы";
                    e.Cancel = true;
                }
            }
        }

        private void unitsData_DataSourceChanged(object sender, EventArgs e)
        {
            unitsData.RowHeadersVisible = false;
            unitsData.Columns["RowError"].Visible = false;
            unitsData.Columns["RowState"].Visible = false;
            unitsData.Columns["Table"].Visible = false;
            unitsData.Columns["HasErrors"].Visible = false;
            unitsData.Columns["KEI"].HeaderText = "Код";
            unitsData.Columns["NAIM"].HeaderText = "Наименование";
            unitsData.Columns["KEI"].Width = 30 * unitsData.Width / 100;
            unitsData.Columns["NAIM"].Width = 70 * unitsData.Width / 100;
            unitsData.Columns["KEI"].ReadOnly = true;
            unitsData.Columns["NAIM"].ReadOnly = false;
        }
    }
}
