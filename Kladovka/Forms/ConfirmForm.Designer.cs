﻿namespace Balans.Forms
{
    partial class ConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.Message = new System.Windows.Forms.Label();
            this.CodeWordTB = new System.Windows.Forms.TextBox();
            this.ConfirmWordTB = new System.Windows.Forms.TextBox();
            this.CanselButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(4, 104);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(160, 25);
            this.ConfirmButton.TabIndex = 1;
            this.ConfirmButton.Text = "Подтвердить";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Message.Location = new System.Drawing.Point(1, 9);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(268, 34);
            this.Message.TabIndex = 1;
            this.Message.Text = "Введите кодовое слово или фразу для \r\nподтверждения действия:";
            // 
            // CodeWordTB
            // 
            this.CodeWordTB.Location = new System.Drawing.Point(97, 46);
            this.CodeWordTB.Name = "CodeWordTB";
            this.CodeWordTB.ReadOnly = true;
            this.CodeWordTB.Size = new System.Drawing.Size(235, 20);
            this.CodeWordTB.TabIndex = 5;
            // 
            // ConfirmWordTB
            // 
            this.ConfirmWordTB.Location = new System.Drawing.Point(97, 72);
            this.ConfirmWordTB.Name = "ConfirmWordTB";
            this.ConfirmWordTB.Size = new System.Drawing.Size(235, 20);
            this.ConfirmWordTB.TabIndex = 1;
            this.ConfirmWordTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ConfirmWordTB_KeyDown);
            // 
            // CanselButton
            // 
            this.CanselButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CanselButton.Location = new System.Drawing.Point(170, 104);
            this.CanselButton.Name = "CanselButton";
            this.CanselButton.Size = new System.Drawing.Size(160, 25);
            this.CanselButton.TabIndex = 2;
            this.CanselButton.Text = "Отменить";
            this.CanselButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Кодовое слово:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Поле для ввода:";
            // 
            // ConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CanselButton;
            this.ClientSize = new System.Drawing.Size(337, 131);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CanselButton);
            this.Controls.Add(this.ConfirmWordTB);
            this.Controls.Add(this.CodeWordTB);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.ConfirmButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Подтверждение";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.Label Message;
        private System.Windows.Forms.TextBox CodeWordTB;
        private System.Windows.Forms.TextBox ConfirmWordTB;
        private System.Windows.Forms.Button CanselButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}