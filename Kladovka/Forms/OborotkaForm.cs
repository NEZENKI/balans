﻿using ColumnsPropertiesNamespace;
using Balans.DataSets;
using Balans.DataSets.ReportModelTableAdapters;
using Balans.Forms;
using Balans.Reports;
using Balans.Structs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Balans.Forms.Params;

namespace Balans
{
    public partial class Oborotka : BaseForm
    {
        public static bool isExist = false;

        string _naimk { get => _currentKlad.NAIMK; }
        decimal _kcexk { get => _currentKlad.KCEXK; }

        //int _month { get => _currentListKlad.RDATE.Month; }
        //int _year { get => _currentListKlad.RDATE.Year; }
        int _month;
        int _year;

        

        public Oborotka(LISTKLAD.LISTKLADRow currentLisKlad, decimal month, decimal year)
        {            
            isExist = true;
            _currentKlad = currentLisKlad;
            InitializeComponent();

            _month = (int)month;
            _year = (int)year;            
        }

        private void Oborotka_Load(object sender, EventArgs e)
        {
            Text = "Оборотка " + _naimk.ToString().Trim(' ') + " по " + _kcexk + " цеху за " + _month.ToString() + '.' + _year.ToString();

            LoadProperties();

            UpdateUnitsData();

            UpdateIndustriesData();

            AddColumnsToTable();
                       
            ResetDataSource();

            this.mainDataGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.MainDataGrid_CellFormatting);

            ost1Check.Checked = ost1Value;

            this.ost1Check.CheckedChanged += new System.EventHandler(this.ost1Check_CheckedChanged);

            this.TopMost = true;
            this.TopMost = false;
            this.Activate();
        }

        override public void PropertiesInitialization()
        {
            columnsProperties = new ColumnsProperties();

            columnsProperties.Add("NCARDS","Номер карты");
            columnsProperties.Add("SIGN", "Обозначение");
            columnsProperties.Add("NAIM", "Наименование");
            columnsProperties.Add("CENA", "Цена");
            columnsProperties.Add("OST1", "Остатон на н/м");
            columnsProperties.Add("OST2", "Остаток на к/м");
            columnsProperties.Add("PRIH", "Приход");
            columnsProperties.Add("units", "Единица измерения");
            columnsProperties.Add("CHET", "Счет");
            columnsProperties.Add("RASH", "Расход");
            columnsProperties.Add("NN", "Номенклатурный номер");
            columnsProperties.Add("RASHC", "Расход вне цеха");
            columnsProperties.Add("Industries", "Отрасль");
            columnsProperties.Add("SUM", "Сумма");
        }

        private void AddDataColumnsColumn()
        {
            DataGridViewComboBoxColumn industriesColumn = new DataGridViewComboBoxColumn { Name = "industries" };
            industriesColumn.DataPropertyName = "INDUSTRY";
            INDUSTRIES.INDUSTRIESRow row = (INDUSTRIES.INDUSTRIESRow)industriesTable.NewRow();
            row.ID = null;
            row.NAME = " ";
            List<INDUSTRIES.INDUSTRIESRow> rowsInds = industriesTable.ToList();
            rowsInds.Add(row);
            industriesColumn.DataSource = rowsInds.OrderBy(i => i.NAME).ToList();
            industriesColumn.ValueMember = "ID";
            industriesColumn.DisplayMember = "NAME";
            mainDataGrid.Columns.Add(industriesColumn);

            DataGridViewComboBoxColumn cbColumn = new DataGridViewComboBoxColumn { Name = "units" };
            cbColumn.DataPropertyName = "KEI";
            UNITS.UNITSRow rowUNITSRow = (UNITS.UNITSRow)unitsTable.NewRow();
            rowUNITSRow.KEI = "";
            rowUNITSRow.NAIM = " ";
            List<UNITS.UNITSRow> rows = unitsTable.ToList();
            rows.Add(rowUNITSRow);
            cbColumn.DataSource = rows.OrderBy(u => u.KEI).ToList();
            cbColumn.ValueMember = "KEI";
            cbColumn.DisplayMember = "KEI";
            mainDataGrid.Columns.Add(cbColumn);
        }

        private void AddColumnsToTable()
        {
            AddDataColumnsColumn();

            DataGridViewColumn renumberingColumn = new DataGridViewTextBoxColumn { Name = "Renumber" };
            renumberingColumn.DefaultCellStyle.NullValue = null;
            mainDataGrid.Columns.Add(renumberingColumn);
            mainDataGrid.Columns["Renumber"].Visible = false;
        }

        private void Oborotka_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveProperties();
            isExist = false;
        }
        /// <summary>
        /// обновление данных KARTOTEKA
        /// </summary>
        public void UpdateKartotekaData()
        {  
            kartotekaTable = kartoteka.GetDataByListklad(_idListKlad);
        }

        /// <summary>
        /// обновление данных units
        /// </summary>
        public void UpdateUnitsData()
        {
            unitsTable = units.GetData();
        }
        public void UpdateIndustriesData()
        {
            industriesTable = industries.GetData();         
        }


        /// <summary>
        /// обновление данных в таблице по идентификаторам кладовок
        /// </summary>
        private void ResetDataSource()
        {
            UpdateKartotekaData();
            
            mainDataGrid.DataSource = _kartotekaDataRowsList;

            SetColumnsIndex();           
        }

        /// <summary>
        /// добавление новой записи
        /// </summary>
        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewRec();
        }
        /// <summary>
        /// добавление новой записи
        /// </summary>
        private void AddNewRec()
        {
            if (MessageBox.Show("Вы уверены что хотите добавить новую запись?", "Добавление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                int ID = Convert.ToInt32(kartoteka.NewID());

               mainDataGrid.DataSource = null;

                AddDataColumnsColumn();

                kartoteka.Update(kartotekaTable.AddKARTOTEKARow(ID, Convert.ToDecimal(_idListKlad), 0, 0, " ", " ", 0, 0, null, 0, 0, 0, 0, DateTime.Now, "1", 0, null, 0));

                ResetDataSource();

                FocusOnLastAdded(ID);                
            }
        }
        /// <summary>
        /// копирование выделеной записи
        /// </summary>
        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneRec();
        }
        /// <summary>
        /// копирование выделеной записи
        /// </summary>
        private void CloneRec()
        {
            if (mainDataGrid.SelectedCells.Count > 0)
            {
                int FirstDisplayedRow = mainDataGrid.FirstDisplayedScrollingRowIndex;

                KARTOTEKA.KARTOTEKARow kartRow;

                decimal ID = (Convert.ToDecimal(kartoteka.NewID()));

                //kartRow = KARTOTEKATable._KARTOTEKA.FindByID(Convert.ToDecimal(mainDataGrid.SelectedCells[0].OwningRow.Cells["ID"].Value));
                kartRow = (KARTOTEKA.KARTOTEKARow)mainDataGrid.SelectedCells[0].OwningRow.DataBoundItem;

                mainDataGrid.DataSource = null;

                AddDataColumnsColumn();

                kartoteka.Update(kartotekaTable.AddKARTOTEKARow(ID, kartRow.ID_LISTKLAD, kartRow.NCARDS, kartRow.NN??0, kartRow.SIGN, kartRow.NAIM, 0, 0, kartRow.KEI, 0, 0, 0, 0, kartRow.DDAT, kartRow.CHET, 0, kartRow.INDUSTRY,0));

                ResetDataSource();

                FocusOnClone(Convert.ToDecimal(ID), FirstDisplayedRow);
            }
            else
            {
                MessageBox.Show("Запись для копирования не выбрана.", "Копирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRec();
        }

        private void DeleteRec()
        {
            if (MessageBox.Show("Вы уверены что хотите удалить запись?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {                
                if (mainDataGrid.SelectedCells.Count > 0)
                {
                    int FirstDisplayedRow = mainDataGrid.FirstDisplayedScrollingRowIndex;
                    int rowIndex = mainDataGrid.SelectedCells[0].RowIndex;

                    KARTOTEKA.KARTOTEKARow row = (KARTOTEKA.KARTOTEKARow)mainDataGrid.SelectedCells[0].OwningRow.DataBoundItem;

                    mainDataGrid.DataSource = null;

                    AddDataColumnsColumn();

                    row.Delete();
                    kartoteka.Update(row);
                    ResetDataSource();

                    if (_kartotekaDataRowsList.Count() > 1)
                    {
                        FocusOnRow(rowIndex > 0 ? rowIndex - 1 : 0, FirstDisplayedRow);
                    }
                }
                else
                {
                    MessageBox.Show("Для удаления выберите запись", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                }               
            }            
        }

        private void удалениеКарточекСНулевымОстаткомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRecWithZeroOst();
        }

        private void DeleteRecWithZeroOst()
        {
            if (MessageBox.Show("Вы уверены что хотите удалить записи с остатком равным 0?", "Удаление", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                List<KARTOTEKA.KARTOTEKARow> forDelete = new List<KARTOTEKA.KARTOTEKARow>();
                foreach (KARTOTEKA.KARTOTEKARow row in _kartotekaDataRowsList)
                {
                    if (row.OST1 == 0)
                    {
                        forDelete.Add(row);
                    }
                }
                foreach (var row in forDelete)
                {
                    row.Delete();
                    kartoteka.Update(row);
                }
                ResetDataSource();
            }
        }
        /// <summary>
        /// Объединение записей с одинаковой ценой, цехом и номером карточки
        /// </summary>
        private void объединитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CombineRows();
        }
        /// <summary>
        /// объединение строк с одинаковыми цехом, картой, ценой
        /// </summary>
        private void CombineRows()
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                mainDataGrid.DataSource = null;
                AddDataColumnsColumn();

                KARTOTEKA.KARTOTEKARow bufRow;
                List<KARTOTEKA.KARTOTEKARow> forDelete = new List<KARTOTEKA.KARTOTEKARow>();
                List<KARTOTEKA.KARTOTEKARow> kartotekaArray = _kartotekaDataRowsList;

                for (int index = 0; index < kartotekaArray.Count() - 1; index++)
                {
                    for (int secondIndex = index + 1; secondIndex < kartotekaArray.Count(); secondIndex++)
                    {
                        if (kartotekaArray[index].CHET.Equals(kartotekaArray[secondIndex].CHET) && kartotekaArray[index].NCARDS.Equals(kartotekaArray[secondIndex].NCARDS) &&
                            kartotekaArray[index].NAIM.Equals(kartotekaArray[secondIndex].NAIM) && kartotekaArray[index].CENA.Equals(kartotekaArray[secondIndex].CENA))
                        {
                            forDelete.Add(kartotekaArray[secondIndex]);

                            bufRow = kartotekaArray[index];
                            
                            bufRow.OST1 = (bufRow.OST1 ?? 0) + (kartotekaArray[secondIndex].OST1 ?? 0);
                            bufRow.PRIH = (bufRow.PRIH ?? 0) + (kartotekaArray[secondIndex].PRIH ?? 0);
                            bufRow.RASH = (bufRow.RASH ?? 0) + (kartotekaArray[secondIndex].RASH ?? 0);
                            bufRow.RASHC = (bufRow.RASHC ?? 0) + (kartotekaArray[secondIndex].RASHC ?? 0);
                            bufRow.OST2 = (bufRow.OST1 ?? 0) + (bufRow.PRIH ?? 0) - (bufRow.RASH ?? 0) - (bufRow.RASHC ?? 0);
                            kartoteka.Update(bufRow);
                        }
                        else
                        {
                            index = secondIndex - 1;
                            break;
                        }
                    }
                }

                for (int index = 0; index < kartotekaArray.Count() - 1; index++)
                {
                    if (kartotekaArray[index].CENA == 0 && kartotekaArray[index].OST1 == 0 && kartotekaArray[index].CHET.Equals(kartotekaArray[index + 1].CHET) &&
                        kartotekaArray[index].NCARDS.Equals(kartotekaArray[index + 1].NCARDS) && kartotekaArray[index].NAIM == kartotekaArray[index + 1].NAIM)
                    {
                        if (!forDelete.Contains(kartotekaArray[index + 1]))
                        {
                            forDelete.Add(kartotekaArray[index]);
                        }
                    }
                }
                foreach (var row in forDelete)
                {
                    row.Delete();
                    kartoteka.Update(row);
                }
                ResetDataSource();
            }
        }      

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Общая информация", "О программе");

        }        

        /// <summary>
        /// поиск по введенному тексту
        /// </summary>
        private void searchText_TextChanged(object sender, EventArgs e)
        {
            FocusOnSearchRow();
        }        

        private void объединениеКарточекToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CombineRows();
        }

        private void копированиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneRec();
        }

        private void вставкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewRec();
        }

        private void удалениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRec();
        }

        private void обнулениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetZeroPrice();
        }

        private void обнулениеЦеныприToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetZeroPrice();
        }

        private void справочникЕдиницИзмеренияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnitsForm unitsForm = new UnitsForm();
            unitsForm.Show();
        }

        private void Recount(KARTOTEKA.KARTOTEKARow row)
        {
            decimal ost2 = (row.OST1 ?? 0) + (row.PRIH ?? 0) - (row.RASH ?? 0) - (row.RASHC ?? 0);
            row.OST2 = ost2;
            row.SUM = (row.OST2 ?? 0) * (row.CENA ?? 0);
            kartoteka.Update(row);
        }

        private void ost1Check_CheckedChanged(object sender, EventArgs e)
        {
            if (ost1Check.Checked)
            {
                ost1Value = true;
            }
            else
            {
                ost1Value = false;
            }
            mainDataGrid.Columns["OST1"].ReadOnly = !ost1Value;
            mainDataGrid.Refresh();
        }

        /// <summary>
        /// событие привязки данных
        /// </summary>
        private void MainDataGrid_DataSourceChanged(object sender, EventArgs e)
        {
            if(mainDataGrid.DataSource != null)
            {
                foreach (var col in columnsProperties.Columns)
                {
                    mainDataGrid.Columns[col.ColumnName].HeaderText = col.DisplayName;
                }
                mainDataGrid.Columns["Renumber"].HeaderText = "Перенумерация";
                mainDataGrid.Columns["ID"].Visible = false;
                mainDataGrid.Columns["ID_LISTKLAD"].Visible = false;
                mainDataGrid.Columns["RowError"].Visible = false;
                mainDataGrid.Columns["RowState"].Visible = false;
                mainDataGrid.Columns["Table"].Visible = false;
                mainDataGrid.Columns["HasErrors"].Visible = false;
                mainDataGrid.Columns["CENAS"].Visible = false;
                mainDataGrid.Columns["DDAT"].Visible = false;
                //mainDataGrid.Columns["KEI"].Visible = false;

                SetColumnsWidth();

                mainDataGrid.Columns["OST1"].ReadOnly = !ost1Value;
                mainDataGrid.Columns["OST2"].ReadOnly = true;
                mainDataGrid.Columns["SUM"].ReadOnly = true;
            }
        }
        private void SetColumnsWidth()
        {
            this.mainDataGrid.ColumnWidthChanged -= new System.Windows.Forms.DataGridViewColumnEventHandler(this.MainDataGrid_ColumnWidthChanged);

            foreach (var prop in columnsProperties.Columns)
            {
                if (prop.ColumnWidth != null)
                {
                    mainDataGrid.Columns[prop.ColumnName].Width = (int)prop.ColumnWidth;
                }
            }

            this.mainDataGrid.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.MainDataGrid_ColumnWidthChanged);
        }
        /// <summary>
        /// упорядочивание столбцов таблицы
        /// </summary>
        private void SetColumnsIndex()
        {
           columnsProperties.ResetIndices();
            foreach (var prop in columnsProperties.Columns)
            {
                if (prop.DisplayIndex != null)
                {
                    mainDataGrid.Columns[prop.ColumnName].DisplayIndex = (int)prop.DisplayIndex;
                }
            }
        }

        private void MainDataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = mainDataGrid.CurrentCell.ColumnIndex;

            if (columnIndex == mainDataGrid.Columns["NCARDS"].Index || columnIndex == mainDataGrid.Columns["NN"].Index || columnIndex == mainDataGrid.Columns["CHET"].Index || columnIndex == mainDataGrid.Columns["Renumber"].Index)
            {               
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress += new KeyPressEventHandler(KeyPressEventInt_DataTable);
            }
            else
            if (columnIndex == mainDataGrid.Columns["PRIH"].Index || columnIndex == mainDataGrid.Columns["RASH"].Index || columnIndex == mainDataGrid.Columns["OST1"].Index ||
                columnIndex == mainDataGrid.Columns["OST2"].Index || columnIndex == mainDataGrid.Columns["CENA"].Index || columnIndex == mainDataGrid.Columns["RASHC"].Index)
            {
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
                e.Control.KeyPress += new KeyPressEventHandler(KeyPressEventFloat_DataTable);
            }
            else
            {
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventInt_DataTable);
                e.Control.KeyPress -= new KeyPressEventHandler(KeyPressEventFloat_DataTable);
            }

        }

        private void переходНаНовыйМесяцToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CombineRows();
            SetZeroPrice();
            GoToNewMonth();// перенос остатков на новй месяц и изменение даты
        }

        private void начатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renumberingValues = new List<Renumbering>();
            mainDataGrid.Columns["Renumber"].Visible = true;
            mainDataGrid.Columns["Renumber"].DisplayIndex = mainDataGrid.Columns["NCARDS"].DisplayIndex + 1;
            fillRenumberingColumnNullValues();
        }

        private void fillRenumberingColumnNullValues()
        {
            foreach(DataGridViewRow item in mainDataGrid.Rows)
            {
                item.Cells["Renumber"].Value = null ;
            }
        }

        private void остаткиПоОтрослямToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaTable.ToArray());

            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_kartotekaDataRowsList.Count() > 0)
                {
                    REMAININDUSTRIESTableAdapter kartoteka = new REMAININDUSTRIESTableAdapter();
                    ReportModel.REMAININDUSTRIESDataTable KARTOTEKATable = new ReportModel.REMAININDUSTRIESDataTable();
                    KARTOTEKATable = kartoteka.GetData((decimal)_idListKlad);
                    RemainMaterialsOborotkaIndustriesRdlc rb = new RemainMaterialsOborotkaIndustriesRdlc(KARTOTEKATable, Convert.ToInt32(_kcexk), _naimk.ToString(), _month, _year);
                }
                else
                {
                    MessageBox.Show("В базе данных оборотки отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void отчетПоИнвентаризацииПоОбороткеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaTable.ToArray());

            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_kartotekaDataRowsList.Count() > 0)
                {
                    InventoryOborotkaReportRdlc rb = new InventoryOborotkaReportRdlc(_kartotekaDataRowsList, Convert.ToInt32(_kcexk), _naimk.ToString());
                }
                else
                {
                    MessageBox.Show("В базе данных оборотки отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void справочникОтрослейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IndustriesForm indstriesForm = new IndustriesForm();
            indstriesForm.Show();
        }

        private void отчетПоОбороткеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_kartotekaDataRowsList.Count() > 0)
                {
                    OborotkaReportRdlc rb = new OborotkaReportRdlc(_kartotekaDataRowsList, Convert.ToInt32(_kcexk), _naimk.ToString(), _month, _year);
                }
                else
                {
                    MessageBox.Show("В базе данных оборотки отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }       

        private void остаткиПоОбороткеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<IncorrectValue> wn = NamesValidation(kartotekaTable.ToArray());
            if (wn.Count() > 0)
            {
                ErroreDataMessage msg = new ErroreDataMessage(wn, "Неверное наименование", "Различные наименования или обозначения при одинаковых карточках:");
                msg.Show();
            }
            else
            {
                if (_kartotekaDataRowsList.Count() > 0)
                {
                    RemainMaterialsOborotkaRdlc rb = new RemainMaterialsOborotkaRdlc(_kartotekaDataRowsList, Convert.ToInt32(_kcexk), _naimk.ToString(), _month, _year);
                }
                else
                {
                    MessageBox.Show("В базе данных оборотки отсутствуют данные.", "Отсутствуют данные", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
            }
        }

        private void завершитьССохранниемИзмененийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите сохранить изменения?", "Завершение перенумерации", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.EndEdit();
                SaveRenumbering();
                mainDataGrid.Columns["Renumber"].Visible = false;
                renumberingValues.Clear();
            }
        }

        private void SaveRenumbering()
        {
            if (renumberingValues.Count > 0)
            {
                for (int i = 0; i < mainDataGrid.RowCount; i++)
                {
                    if (mainDataGrid["Renumber", i].Value!= null)
                    {
                        mainDataGrid["NCARDS", i].Value = Convert.ToDecimal(mainDataGrid["Renumber", i].Value);
                    }
                }
            }
        }

        private void завершитьБезСохраненияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены что хотите завершить операцию без сохранения?", "Завершение перенумерации", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                mainDataGrid.Columns["Renumber"].Visible = false;
                renumberingValues.Clear();
            }
        }

        private void mainDataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && mainDataGrid.Columns[e.ColumnIndex].Visible)
            {
                var row = (KARTOTEKA.KARTOTEKARow)mainDataGrid.Rows[e.RowIndex].DataBoundItem;
                if (e.ColumnIndex == mainDataGrid.Columns["Renumber"].Index)
                {
                    if (!String.IsNullOrEmpty(mainDataGrid["Renumber", e.RowIndex].Value.ToString()))
                    {
                        renumberingValues.Add(new Renumbering { ID = Convert.ToInt32(mainDataGrid["ID", e.RowIndex].Value), newNCards = Convert.ToInt32(mainDataGrid["Renumber", e.RowIndex].Value) });
                    }
                    else
                    {
                        var item = renumberingValues.Find(r => r.ID == Convert.ToInt32(mainDataGrid["ID", e.RowIndex].Value));
                        renumberingValues.Remove(item);
                    }
                }
                if (new string[]{
                    "CENA", "PRIH", "OST1", "RASH", "RASHC" }
                .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
                {
                    Recount(row);
                    mainDataGrid.Refresh();
                }
                row.DDAT = DateTime.Now;                
                kartoteka.Update(row);
                if (new string[] {
                    "CENA", "NAIM", "SIGH", "NCARDS" }
                .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        ResortAfterEdit();
                    }));
                }
            }

        }

        //числа с дробной частью
        private void KeyPressEventFloat_DataTable(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        //числа без дробной части
        private void KeyPressEventInt_DataTable(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void шапкаToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            RashNames rashNames = new RashNames(Convert.ToDecimal(_idListKlad));
            rashNames.Show();
        }

        private void searchText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void минимальнаяЗарплатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MinSalary salaryForm = new MinSalary(seting);
            salaryForm.ShowDialog();
        }     
        private void GoToNewMonth()
        {//перенос остатков на новый месяц
            if (MessageBox.Show("Вы уверены что хотите перенести остатки на начало месяца?\nДанные о приходах и расходах будут удалены.", "Перенос остатков", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                foreach (var item in _kartotekaDataRowsList)
                {
                    item.OST1 = item.OST2;
                    item.PRIH = 0;
                    item.RASH = 0;
                    kartoteka.Update(item);
                }
                ResetDataSource();
            }
        }

        //окрашиваем каждую вторую строку в таблице в другой цвет
        private void MainDataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (Convert.ToInt32(mainDataGrid["OST2", e.RowIndex].Value) < 0)
            {
                e.CellStyle.BackColor = Color.IndianRed;
            }
            else if (e.ColumnIndex == mainDataGrid.Columns["PRIH"].Index)
            {
                e.CellStyle.BackColor = Color.LightSkyBlue;
            }
            else
            {
                if (e.RowIndex % 2 == 0 && e.ColumnIndex != mainDataGrid.Columns["units"].Index)
                {
                    e.CellStyle.BackColor = Color.Bisque;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["OST1"].Index && !ost1Check.Checked)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["OST2"].Index || e.ColumnIndex == mainDataGrid.Columns["NCARDS"].Index)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                if (e.ColumnIndex == mainDataGrid.Columns["Renumber"].Index)
                {
                    e.CellStyle.BackColor = Color.BurlyWood;
                }
            }
            if (new string[] { "CENA", "PRIH", "OST1", "OST2", "RASH", "RASHC", "NCARDS"}
               .Contains(mainDataGrid.Columns[e.ColumnIndex].Name))
            {
                e.CellStyle.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);
            }
        }

        /// <summary>
        /// изменение размеров столбцов
        /// </summary>
        private void MainDataGrid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
             Column column = columnsProperties.FindByName(e.Column.Name);
            if (column != null)
            {
                column.ColumnWidth = e.Column.Width;
            }
        }

        private void обновитьДанныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = mainDataGrid.SelectedCells.Count > 0? mainDataGrid.SelectedCells[0].RowIndex : 0;
            int firstDisplayed = mainDataGrid.FirstDisplayedScrollingRowIndex;
            UpdateKartotekaData();
            ResetDataSource();
            //выделяем обратно строку
            FocusOnRow(rowIndex, firstDisplayed);
        }       
        /// <summary>
        /// показывает пользователю на экране последний добавленный элемент
        /// </summary>
        private void FocusOnLastAdded(int ID)
        {
            for (int i = 0; i < mainDataGrid.Rows.Count; i++)
            {
                if (mainDataGrid["ID", i].Value.Equals(ID))
                {
                    FocusOnRow(i, i);
                    break;
                }
            }
        }

        private void FocusOnClone(decimal ID, int firstDisplayed)
        {
            for (int i = 0; i < mainDataGrid.Rows.Count; i++)
            {
                if (mainDataGrid["ID", i].Value.Equals(ID))
                {
                    FocusOnRow(i, firstDisplayed);
                    break;
                }
            }
        }


        private void FocusOnSearchRow()
        {
            if (decimal.TryParse(searchText.Text, out decimal buf))
            {
                for (int i = 0; i < mainDataGrid.Rows.Count; i++)
                {
                    if (mainDataGrid.Rows[i].Cells["NCARDS"].Value.Equals(buf))
                    {
                        FocusOnRow(i, i);
                        break;
                    }
                }
            }
            else
            {
                searchText.Text = "";
            }
        }

        private void FocusOnRow(int focusedRow, int firstDisplayedRow)
        {
            if (focusedRow > 0 && mainDataGrid.Rows.Count >= focusedRow)
            {
                mainDataGrid.Rows[focusedRow].Cells["NAIM"].Selected = true;
                mainDataGrid.FirstDisplayedScrollingRowIndex = firstDisplayedRow;
            }
        }

        /// <summary>
        /// устанавливает цену в 0 при остатке равном 0
        /// </summary>
        private void SetZeroPrice()
        {
            foreach (KARTOTEKA.KARTOTEKARow row in _kartotekaDataRowsList)
            {
                if (Convert.ToDecimal(row.OST1) == 0)
                {
                    row.CENA = 0;
                    row.RASH = 0;
                    row.PRIH = 0;
                    row.RASHC = 0;
                    kartoteka.Update(row);
                }
            }
            ResetDataSource();
        }

        protected override IniFile GetSettingsFile()
        {
            Directory.CreateDirectory("C:\\Users\\Public\\properties_MSP");
            return new IniFile("C:\\Users\\Public\\properties_MSP\\oborotka.ini");            
        }
        private void ResortAfterEdit()
        {
            var id = mainDataGrid.SelectedCells[0].OwningRow.Cells["ID"].Value;
            var columnName = mainDataGrid.SelectedCells[0].OwningColumn.Name;
            var rowInd = mainDataGrid.FirstDisplayedScrollingRowIndex;

            mainDataGrid.DataSource = _kartotekaDataRowsList;

            SetColumnsIndex();

            foreach (DataGridViewRow gridRow in mainDataGrid.Rows)
            {
                if (gridRow.Cells["ID"].Value.Equals(id))
                {
                    gridRow.Cells[columnName].Selected = true;
                    mainDataGrid.FirstDisplayedScrollingRowIndex = rowInd;
                    break;
                }
            }
        }

        private void настройкаОтображенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColumnsOrder co = new ColumnsOrder(columnsProperties);
            co.ShowDialog();
            SetColumnsIndex();
        }
    }
}